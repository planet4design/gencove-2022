import styles from 'styles/about-company.module.scss'
import cn from 'classnames'

function CompanyContentTwo() {
  return (
    <div className={styles.wrapSmaller}>
      <div className={cn('container', styles.content)}>
        
        <div className="row">

          <div className="col-lg">
            <h2 className={styles.titleSmaller}>The next revolution in genomics will be driven by software</h2>
            <p className={styles.subTitle}>
              Hardware cost, speed and capacity are continually improving, however, the sequencing machines are only one part of the genomics equation. Sequencing large populations generates massive amounts of data. For genomics to be universally accessible, a software platform is needed to help interpret that data to support scientific discovery.
            </p>
          </div>
          <div className="col-lg">
            <img src="/img/company-image-1.jpg" />
          </div>

        </div>

      </div>
    </div>
  )
}

export default CompanyContentTwo
