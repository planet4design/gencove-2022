import Link from 'next/link'
import styles from 'styles/hero.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'

function Hero() {
  return (
    <div className={cn(styles.wrap, 'hero')}>
      <video className={styles.video} muted playsInline loop autoPlay poster="img/bg-hero-home.jpg">
        <source src="video/home-hero-video.webm" type="video/webm" />
        <source src="video/home-hero-video.mp4" type="video/mp4" />
      </video>
      <div className={styles.overlay}></div>
      <div className="container">
        <div className={styles.content}>
          <RevealText waitFont={false}>
            <h1 className={styles.title}>GENOMIC TECHNOLOGY</h1>
          </RevealText>
          <RevealText waitFont={false}>
          <p className={styles.subTitle}>to advance global health and sustainability</p>
          </RevealText>
          <RevealText waitFont={false}>
            <Link href="/products">
            <a href="/products" className={styles.link}>
                <p>Explore the possibilities <span className={styles.outlineArrow}>&gt;</span></p>
            </a>
          </Link>
          </RevealText>
        </div>
      </div>
    </div>
  )
}

export default Hero
