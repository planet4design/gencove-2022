import styles from 'styles/about-company.module.scss'
import RevealText from 'components/reveal-text'

function QuoteTwo() {
  return (
    <div className={styles.wrapQuote}>
      <div className="container">

        <div className="row">
          <div className="col-lg-1">
            <RevealText>
              <img src="/img/testimonials/quotes.png" alt="quotes" />
            </RevealText>
          </div>
          <div className="col-lg-11">
            <RevealText>
              <h4 className={styles.quote}>
                Low-pass whole genome sequencing is an important step but to advance global health and sustainability, we need to create the software to associate genotypic variation to phenotypic outcomes - that’s our goal.
              </h4>
            </RevealText>
            <p className={styles.quoter}>
              <span className={styles.bold}>Joseph Pickrell</span><br />
              Co-founder and CEO
            </p>
          </div>
        </div>

      </div>
    </div>
  )
}

export default QuoteTwo
