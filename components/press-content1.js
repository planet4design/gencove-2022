import styles from 'styles/press.module.scss'
import cn from 'classnames'

function CompanyContentTwo() {
  return (
    <div className={styles.wrapSmaller}>
      <div className={cn('container', styles.content)}>
        
        <div className="row">

          <div className="col-lg">

            <p className={styles.subTitle}>January 26, 2022  -  Gencove</p>

            <h2 className={styles.titleSmaller}>BGI and Gencove Extend Agreement to Offer Low-Pass Whole Genome Sequencing and Analysis Services to Advance Global Health and Sustainability</h2>
            
            <p className={styles.subTitle}>
              Partnership delivers high-volume, cost-efficient, low-pass whole genome sequencing for human genetics and other applications.
              </p>

            <p className={styles.subTitle}>
<strong>NEW YORK and CAMBRIDGE, MA.</strong> – Jan. 10, 2022 /GLOBENewswire/ – Gencove, a pioneer of low-pass whole genome sequencing (LPWGS) analysis, and BGI Americas Corporation, US subsidiary of BGI Genomics Co. Ltd. (300676.SZ), one of the world&rsquo;s leading multi-omics companies, today announced the extension of the subscription and joint marketing agreements. The partnership combines BGI&quot;s sequencing services from proprietary DNBSEQ™ sequencing technology with Gencove&rsquo;s analysis platform so that high-capacity, cost-efficient whole genome information can be made more accessible and actionable. Both companies are united by a common goal of providing genomic information for a healthier and more sustainable world.</p>

<p className={styles.subTitle}>&ldquo;Improving hardware cost, speed, and capacity is one part of the equation&rdquo;, said Joseph Pickrell, Ph.D., co-founder, and CEO of Gencove. &ldquo;Sequencing large populations generates massive amounts of data. We knew that for genomics to be universally applicable, we needed to help interpret that data to support scientific discovery.&rdquo;</p>

<p className={styles.subTitle}>Gencove developed a unique software platform that makes whole genome sequencing fast, affordable, and the data more understandable. Gencove&rsquo;s technology platform is called low-pass whole genome sequencing (LPWGS). Requiring less DNA per sample and using any sequencing machine, the whole genome is read but fewer times. In a step called imputation, the lower depth reads are compared to a fully characterized reference genome. The data is further analyzed depending on the customer&rsquo;s needs, but the result is highly accurate with reliable genomic information.</p>

<p className={styles.subTitle}>&ldquo;Gencove&rsquo;s platform helps us fulfill our mission of providing the highest quality genomic data at industry-leading pricing&rdquo;, said Charles Bao, Ph.D., general manager of BGI Americas. &ldquo;The combination of DNBSEQ™ technology-based sequencing with Gencove&rsquo;s software brings personalized medicine and bio-sustainability much closer to reality. Since BGI and Gencove launched our combined LPWGS solution more than three years ago, we have seen successful adoption and application by many of our customers across a wide range of human, plant, and animal research.&rdquo;</p>

<p className={styles.subTitle}>With over 20 years of experience, BGI Genomics is one of the world&rsquo;s leading providers of genomic sequencing services and proteomic services. The company provides highly affordable, integrated services across a broad range of applications, including:</p>

            <ul className={styles.bulletList}>
              <li className={styles.bulletListItem}>
                Basic research for human, plant, animal, and microbial species
              </li>
              <li className={styles.bulletListItem}>
                Drug discovery and development
              </li>
              <li className={styles.bulletListItem}>
                Agriculture and biodiversity preservation and sustainability
              </li>
            </ul>

<p className={styles.subTitle}>The partnership means academic institutions, biotechnology, diagnostic, and pharmaceutical companies all over the world can utilize the scale and efficiency of low-pass whole genome sequencing from two trusted leaders in genomics.</p>

<p className={styles.subTitle}><strong>About Gencove</strong></p>

<p className={styles.subTitle}>With additional threats to human health and the challenge of feeding the world&rsquo;s growing population, demand for genomic information has skyrocketed. Historically, DNA processing options fell into two categories: low-throughput and costly options such as whole genome sequencing, and high-throughput but limited technologies like genotyping arrays. To help scientists make life-saving discoveries, Gencove combines low-pass whole genome sequencing (reading the DNA at shallow depths) with a proprietary software-as-a-service (SaaS) computation layer. The result is a fast, high-volume, and cost-sensitive sequencing solution. As evidence of the technology&rsquo;s value, the company has hundreds of customers, and the largest genomics service providers in the world partner with Gencove.&nbsp; For more information visit: www.gencove.com and follow us on www.linkedin.com/company/gencove/mycompany/ and twitter.com/Gencove.</p>

<p className={styles.subTitle}><strong>About BGI Americas Corporation</strong></p>

<p className={styles.subTitle}>BGI Americas Corporation is one of the leading providers of genomics and proteomics services in the Americas Region as part of BGI Genomics, a public company listed on the Shenzhen Stock Exchange. Established in 2010, BGI Americas has grown to include a presence in Boston and San Jose, serving a wide range of customers in genetics research, drug R&amp;D, and diagnostics. BGI brings over 20 years of genomics experience to its customers and collaborators based on a broad array of leading technologies, including proprietary DNBSEQ genetic sequencing platforms, economies of scale, and expert bioinformatics resources. BGI is committed to advancing genetic research, technologies, and applications to benefit humankind.</p>

<p className={styles.subTitle}>Contacts</p>
<p className={styles.subTitle}>For Gencove:</p>
<p className={styles.subTitle}>Kristi Ashton, 650.224.8231</p>
<p className={styles.subTitle}>kristi.ashton@gencove.com</p>
<p className={styles.subTitle}>For BGI Americas:</p>
<p className={styles.subTitle}>Anthony Tong, 617.500.2741</p>
<p className={styles.subTitle}>anthony.tong@bgi.com</p>
<p className={styles.subTitle}>For BGI Genomics (Global markets excl. Americas):</p>
<p className={styles.subTitle}>Matt Poulter</p>
<p className={styles.subTitle}>matt.poulter@bgi.com</p>



      </div>
    </div>
      </div>
    </div>
  )
}

export default CompanyContentTwo
