import styles from 'styles/testimonials.module.scss'
import RevealText from 'components/reveal-text'
import { useInView } from 'react-hook-inview'

function Testimonials() {
  const [ref, inView] = useInView({ threshold: 0.5, unobserveOnEnter: true })

  const testimonial = {
    name: 'Prof. Eran Segal',
    position: 'Principal Investigator at Weizmann Institute of Science',
    opinion:
      "Due to the large number of assays that we perform, each individual assay is optimized both operationally and for cost. Gencove's low-pass sequencing platform has allowed us to gather accurate genome-wide genetic data and oral microbiome data."
  }

  return (
    <div ref={ref} id="testimonials" className={styles.wrap}>
      <div className={styles.lines}>
        <div className="container">
          <blockquote className={styles.content}>
            <RevealText stagger={0.05}>
              <p className={styles.paragraph}>{testimonial.opinion}</p>
            </RevealText>
            <cite>
              <p className={styles.name}>{testimonial.name}</p>
              <span className={styles.position}>{testimonial.position}</span>
            </cite>
          </blockquote>
        </div>
      </div>
    </div>
  )
}

export default Testimonials

