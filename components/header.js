import Link from 'next/link'
import { useState, useEffect } from 'react'
import cn from 'classnames'
import Logo from 'components/logo'
import IconClose from 'components/icons/close'
import IconMenu from 'components/icons/menu'
import styles from 'styles/header.module.scss'
import { useRouter } from 'next/router'
import Headroom from 'react-headroom'
import { useWindowWidth } from '@react-hook/window-size'

function Header(props) {
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const [isScrolled, setIsScrolled] = useState(false)
  const [windowWidht, setWindowWidht] = useState()
  const [heroHeight, setHeroHeight] = useState(0)
  const router = useRouter()
  const toggle = () => setIsMenuOpen(!isMenuOpen)
  const widthSize = useWindowWidth()
  const smWindow = 576

  // hide scrollbar when menu is open
  useEffect(() => {
    if (isMenuOpen) {
      document.querySelector('body').classList.add('overflow-hidden')
    }
    return () => {
      document.querySelector('body').classList.remove('overflow-hidden')
    }
  }, [isMenuOpen])

  // change navbar colors acording to scroll
  let listener = null
  useEffect(() => {
    listener = document.addEventListener('scroll', e => {
      const scrolled = document.scrollingElement.scrollTop

      if (document.querySelector('.hero') != null) {
        const heroHeight = document.querySelector('.hero').clientHeight
        const headerHeight = document.getElementById('header').clientHeight
        const deltaHeight = heroHeight - headerHeight

        if (scrolled > deltaHeight) {
          setIsScrolled(true)
          document.getElementById('header').style.position = 'fixed'
        } else {
          setIsScrolled(false)
          document.getElementById('header').style.position = 'absolute'
        }
      } else {
        setHeroHeight(0)
      }
    })
    return () => {
      document.removeEventListener('scroll', listener)
    }
  }, [isScrolled])

  //get hero height for pinStart
  useEffect(() => {
    if (document.querySelector('.hero') != null) {
      const heroHeight = document.querySelector('.hero').clientHeight
      const headerHeight = document.getElementById('header').clientHeight
      const deltaHeight = heroHeight - headerHeight
      setHeroHeight(deltaHeight)
    }
  }, [])

  // get device width for sign in color in contact-us navbar
  useEffect(() => {
    if (router.pathname == '/contact-us') {
      const width = document.querySelector('body').clientWidth
      setWindowWidht(width)
    }
  }, [])
  const signInBlue = router.pathname == '/contact-us' && windowWidht > 752

  return (
    <Headroom
      pinStart={heroHeight}
      //disable={router.pathname == '/contact-us' ? true : isMenuOpen ? true : false}
      disable={true}
      disableInlineStyles
    >
      <div
        id="header"
        className={cn(styles.wrap, isScrolled && styles.bgWhite, isMenuOpen && styles.headerMenu)}
      >
        <div
          className={cn(styles.content, router.pathname == '/contact-us' && styles.contactContent)}
        >
          <div className={styles.nav}>
            <Link href="/">
              <a>
                <span className="sr-only">Gencove</span>
                <Logo
                  className={cn(
                    styles.logo,
                    isScrolled ? styles.colorLight : styles.colorLight,
                    isMenuOpen && styles.colorLight
                  )}
                  width="154"
                  height="44"
                />
              </a>
            </Link>
            <button onClick={toggle} className={styles.menu}>
              {isMenuOpen ? (
                <IconClose width="24" height="24" />
              ) : (
                <IconMenu
                  className={isScrolled ? styles.colorLight : styles.colorLight}
                  width="24"
                  height="24"
                />
              )}
            </button>
          </div>
          <div className={cn(styles.menuMobile, isMenuOpen ? styles.menuBg : styles.links)}>
            <div className={cn(styles.btnContainer, isMenuOpen ? 'd-flex' : 'd-block')}>
    

                            
                <Link href="/products">
                  <a
                    className={
                      isMenuOpen
                        ? cn(styles.btn, styles.btnBlue)
                        : cn(styles.link, isScrolled && styles.linkBlue)
                    }
                  >
                    Products
                  </a>
                </Link>
                         
              
                <Link href="/human-genomics">
                  <a
                    className={
                      isMenuOpen
                        ? cn(styles.btn, styles.btnBlue)
                        : cn(styles.link, isScrolled && styles.linkBlue)
                    }
                  >
                    Human Genomics
                  </a>
                </Link>

              
                <Link href="/agrigenomics">
                  <a
                    className={
                      isMenuOpen
                        ? cn(styles.btn, styles.btnBlue)
                        : cn(styles.link, isScrolled && styles.linkBlue)
                    }
                  >
                    Agrigenomics
                  </a>
                </Link>
              
              
                <Link href="/company">
                  <a
                    className={
                      isMenuOpen
                        ? cn(styles.btn, styles.btnBlue)
                        : cn(styles.link, isScrolled && styles.linkBlue)
                    }
                  >
                    Company
                  </a>
                </Link>
              
            

              
              <Link href="https://web.gencove.com/">
                  <a
                    className={
                      isMenuOpen
                        ? cn(styles.btn, styles.btnBlue)
                        : cn(styles.link, isScrolled && styles.linkBlue)
                    }
                  >
                  Sign in
                </a>
              </Link>  
              
              
                <Link href="/contact-us">
                  <a
                    className={
                      isMenuOpen
                        ? cn(styles.btn, styles.btnBlue)
                        : cn(styles.link, isScrolled && styles.linkBlue)
                    }
                  >
                    Contact us
                  </a>
                </Link>

              
            </div>
          </div>
        </div>
      </div>
    </Headroom>
  )
}

export default Header
