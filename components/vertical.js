import styles from 'styles/about-agri.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'

function AgriContentTwo() {
  return (
    <div className={styles.wrapSmaller}>
      <div className={cn('container', styles.content)}>

        <div className="row">

          <div className="col-lg">
            <img src="/img/home-hg.jpg" className={styles.img} />
          </div>

          <div className="col-lg">
            <h2 className={styles.titleSmaller}>Human genomics</h2>
            <h2 className={styles.subTitle}>To power your research, we developed revolutionary software that makes whole-genome sequencing fast, affordable, and actionable.</h2>
            <ul className={styles.bulletList}>
              <li className={styles.bulletListItem}>
                Make life-saving discoveries
              </li>
              <li className={styles.bulletListItem}>
                Accelerate genotypic and phenotypic associations
              </li>
              <li className={styles.bulletListItem}>
                Give more information to your customers
              </li>
            </ul>
            <h3>
            <a href="/human-genomics" className={styles.btnBlue}>See how <span className={styles.outlineArrowBlue} >&gt; </span></a>
            </h3>
          </div>

        </div>
        
        <div className="row">

          <div className="col-lg">
            <img src="/img/home-ag.jpg" className={styles.img} />
          </div>
          <div className="col-lg">
            <h2 className={styles.titleSmaller}>Agrigenomics</h2>
            <h2 className={styles.subTitle}>Genomics is critical for diverse and sustainable plant and animal breeding, especially as the climate and consumer preferences change. With Gencove, one
assay can replace all common arrays.</h2>
            <ul className={styles.bulletList}>
              <li className={styles.bulletListItem}>
                Accelerate breeding efficiency
              </li>
              <li className={styles.bulletListItem}>
                Achieve a competitive advantage
              </li>
              <li className={styles.bulletListItem}>
                Confidently select the best-in-breed
              </li>
            </ul>
            <h3>
            <a href="/agrigenomics" className={styles.btnBlue}>Learn more <span className={styles.outlineArrowBlue} > &gt;</span></a>
            </h3>
          </div>

        </div>

      </div>
    </div>
  )
}

export default AgriContentTwo
