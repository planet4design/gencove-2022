import styles from 'styles/partners.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'


function Partners(props) {
  return (
    <div className={styles.wrap}>
      <div className={cn('container', styles.content)}>
      
      <div className="row">
                  <h2>BGI and Gencove Extend Agreement to Offer Low-Pass Whole Genome Sequencing and Analysis Services to Advance Global Health and Sustainability</h2>
      </div>



      <div className="row">
                  <h2>To offer the most advanced sequencing solutions, the largest genomics service providers in the world partner with Gencove.</h2>
      </div>


      </div>
    </div>
  )
}

export default Partners
