import Button from 'components/button'
import styles from 'styles/contact.module.scss'
import cn from 'classnames'
import Form from 'components/form'
import RevealText from 'components/reveal-text'
import Shape from './circular-lines-black'

function Contact() {
  return (
    <div className={cn(styles.wrap)}>
      <h1 className="sr-only">Contact us</h1>
      <div className={styles.leftBox}>
        <div className={styles.bgDark}>
          <Shape />
          <div className={styles.texts}>
            <RevealText>
              <h2 className={styles.title}>We&rsquo;re eager to hear from you</h2>
            </RevealText>
            <p className={styles.subtitle}>
              Complete the form and we will reach out to you right away.
            </p>
          </div>
        </div>
      </div>
      <div className={styles.rightBox}>
        <Form />
      </div>
    </div>
  )
}

export default Contact
