import styles from 'styles/about-agri.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'

function AgriContentTwo() {
  return (
    <div className={styles.wrapSmaller}>
      <div className={cn('container', styles.content)}>

        <div className="row">

          <div className="col-lg">
            <h2 className={styles.titleSmaller}>Accelerate breeding progress</h2>
            <ul className={styles.bulletList}>
              <li className={styles.bulletListItem}>
                Genomic selection
              </li>
              <li className={styles.bulletListItem}>
                Marker-assisted breeding
              </li>
              <li className={styles.bulletListItem}>
                QTL mapping
              </li>
              <li className={styles.bulletListItem}>
                Parentage analysis
              </li>
              <li className={styles.bulletListItem}>
                Breed or variety analysis
              </li>
              <li className={styles.bulletListItem}>
                Embryo genotyping
              </li>
              <li className={styles.bulletListItem}>
                Trait testing
              </li>
              <li className={styles.bulletListItem}>
                Diagnostics
              </li>
            </ul>
          </div>
          <div className="col-lg">
            <img src="/img/agri-img-1.jpg" className={styles.img} />
          </div>

        </div>
        
        <div className="row">

          <div className="col-lg">
            <img src="/img/agri-img-2.jpg" className={styles.img} />
          </div>
          <div className="col-lg">
            <h2 className={styles.titleSmaller}>Achieve a competitive edge</h2>
            <ul className={styles.bulletList}>
              <li className={styles.bulletListItem}>
                Confidently select the best-in-breed
              </li>
              <li className={styles.bulletListItem}>
                Increase the value of your population
              </li>
            </ul>
            <h3>
              <a href="/products" className={styles.btnBlue}>See all our products have to offer <span className={styles.outlineArrowBlue} >&gt;</span></a>
            </h3>
          </div>

        </div>

      </div>
    </div>
  )
}

export default AgriContentTwo
