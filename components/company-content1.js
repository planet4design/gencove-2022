import styles from 'styles/black-section.module.scss'

function CompanyBlack() {
  return (
    <div className={styles.wrap}>
      <div className="container">
        <div className="row">
          <div className="col-lg">
            <p className={styles.subTitle}>
              DNA sequencing has become the foundation for biological advancements by enabling exploration of genetic variants and structural changes in DNA. Consequently, demand for large-scale genomics is significantly increasing. Historically, processing DNA fell into two categories: low-throughput and costly options such as whole genome sequencing and high-throughput but limited technologies like genotyping arrays. Those two options are insufficient to help scientists make life-saving discoveries at the scale and pace needed.
            </p>
          </div>
          <div className="col-lg">
          <p className={styles.subTitle}>
            That’s why we developed a new technology. Gencove combines low-pass whole genome sequencing (shallower DNA reads) with a proprietary software-as-a-service computation layer. The result is a hardware-agnostic, high-volume, and cost-sensitive sequencing solution. With Gencove’s software, academic institutions, agriculture, biotechnology, diagnostic and pharmaceutical companies can now get complete genomic information.
          </p>
        </div>
        </div>
      </div>
    </div>
  )
}

export default CompanyBlack
