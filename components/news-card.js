import Link from 'next/link'
import cn from 'classnames'
import styles from 'styles/news-card.module.scss'

function NewsCard({ article }) {
  return (
    <a href={article.url} className={styles.link} target="_blank" rel="noopener noreferrer">
      <div
        className={cn(
          styles.card,
          article.variant === 'Guide'
            ? styles.caseStudy
            : article.variant === 'Paper'
            ? styles.paper
            : article.variant === 'White-Paper' && styles.whitePaper
        )}
      >
        <div className={styles.content}>
          <div className={styles.variant}>{article.variant}</div>
          <h4 className={styles.title}>{article.title}</h4>
        </div>
        <img className={styles.img} src={article.icon} alt="" />
        <span className={styles.date}>{article.date} </span>
      </div>
    </a>
  )
}

export default NewsCard
