import styles from 'styles/sequencing.module.scss'
import RevealText from 'components/reveal-text'

function Stripe4() {
  return (
    <g id="text-stripe-4" className="v-hidden">
      <g>
        <text
          fill="black"
          style={{ whiteSpace: 'pre' }}
          fontFamily="Inter"
          fontSize="24"
          fontWeight="bold"
          letterSpacing="-0.03em"
        >
          <tspan x="0" y="24.7273">
            Whole Genome Sequencing
          </tspan>
        </text>
        <g>
          <text
            fill="black"
            style={{ whiteSpace: 'pre' }}
            fontFamily="Inter"
            fontSize="18"
            fontWeight="300"
            letterSpacing="-0.03em"
          >
            <tspan x="20" y="67.5455">
              Measured
            </tspan>
          </text>
          <rect x="2" y="57" width="10" height="10" fill="#0044FF" />
        </g>
      </g>
    </g>
  )
}

function Stripe3() {
  return (
    <g id="text-stripe-3" className="v-hidden">
      <g>
        <text
          fill="black"
          style={{ whiteSpace: 'pre' }}
          fontFamily="Inter"
          fontSize="24"
          fontWeight="bold"
          letterSpacing="-0.03em"
        >
          <tspan x="0" y="24.7273">
            Targeted Low-Pass Sequencing
          </tspan>
        </text>
        <g>
          <text
            fill="black"
            style={{ whiteSpace: 'pre' }}
            fontFamily="Inter"
            fontSize="18"
            fontWeight="300"
            letterSpacing="-0.03em"
          >
            <tspan x="20" y="67.5455">
              Measured
            </tspan>
          </text>
          <rect x="2" y="57" width="10" height="10" fill="#0044FF" />
        </g>
        <g>
          <text
            fill="black"
            style={{ whiteSpace: 'pre' }}
            fontFamily="Inter"
            fontSize="18"
            fontWeight="300"
            letterSpacing="-0.03em"
          >
            <tspan x="150" y="67.5455">
              Imputed
            </tspan>
          </text>
          <rect x="132" y="57" width="10" height="10" fill="#1BECEC" />
        </g>
      </g>
      <g>
        <text
          fill="black"
          style={{ whiteSpace: 'pre' }}
          fontFamily="Inter"
          fontSize="18"
          fontWeight="300"
          letterSpacing="-0.03em"
        >
          <tspan x="268" y="67.5455">
            Targeted Sequencing
          </tspan>
        </text>
        <rect x="250" y="57" width="10" height="10" fill="black" />
      </g>
    </g>
  )
}

function Stripe2() {
  return (
    <g id="text-stripe-2" className="v-hidden">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M448.813 22.5874C448.813 23.4176 448.433 24.1546 447.844 24.6279L450.429 35.4642H470.324V37.5342H451.494L453.623 40.9483C453.757 40.9253 453.893 40.9107 454.032 40.9107C455.417 40.9107 456.539 42.0691 456.539 43.4981C456.539 44.9269 455.417 46.0855 454.032 46.0855C452.648 46.0855 451.526 44.9269 451.526 43.4981C451.526 42.9718 451.679 42.4826 451.94 42.0742L449.567 38.2698L440.861 49.2145C441.043 49.5729 441.147 49.9802 441.147 50.4126C441.147 51.8417 440.025 53 438.641 53C437.256 53 436.134 51.8417 436.134 50.4126C436.134 48.9836 437.256 47.8253 438.641 47.8253C438.868 47.8253 439.088 47.8591 439.297 47.918L448.026 36.9468L432.716 31.2793C432.257 31.8088 431.591 32.1444 430.848 32.1444C429.464 32.1444 428.342 30.9864 428.342 29.5571C428.342 28.1286 429.464 26.97 430.848 26.97C432.153 26.97 433.223 27.9985 433.343 29.3123L448.209 34.8157L445.901 25.1377C444.71 24.9373 443.8 23.8733 443.8 22.5874C443.8 21.1583 444.922 20 446.306 20C447.691 20 448.813 21.1583 448.813 22.5874ZM483.125 33.1647H483.162L483.255 32.1284H485.17V41.5129C485.17 42.0516 485.127 42.5144 485.041 42.9008C484.955 43.2876 484.829 43.63 484.663 43.9277C484.467 44.2763 484.217 44.5806 483.913 44.8405C483.609 45.1004 483.262 45.3159 482.872 45.4871C482.482 45.6583 482.054 45.7865 481.587 45.8722C481.121 45.9577 480.626 46.0004 480.104 46.0004C479.81 46.0004 479.521 45.9894 479.239 45.9672C478.956 45.945 478.691 45.9134 478.442 45.8722C478.193 45.8309 477.966 45.7835 477.76 45.7295C477.555 45.6756 477.381 45.6202 477.24 45.5631V43.7281C477.645 43.8677 478.07 43.9737 478.516 44.0467C478.961 44.1195 479.429 44.156 479.92 44.156C480.897 44.156 481.652 43.961 482.186 43.5713C482.72 43.1816 482.987 42.5904 482.987 41.798V41.5794C482.987 41.4526 482.989 41.2957 482.992 41.1086C482.995 40.922 483.003 40.7651 483.015 40.638H482.996C482.862 40.7521 482.706 40.8649 482.531 40.9755C482.356 41.0864 482.164 41.1881 481.956 41.2798C481.747 41.3717 481.517 41.4448 481.265 41.4986C481.013 41.5524 480.755 41.5794 480.491 41.5794C480.018 41.5794 479.556 41.4921 479.105 41.3179C478.654 41.1435 478.253 40.8679 477.903 40.4906C477.553 40.1139 477.271 39.6303 477.056 39.0407C476.841 38.4512 476.733 37.7415 476.733 36.911C476.733 36.0807 476.842 35.3532 477.06 34.729C477.278 34.1048 477.568 33.5831 477.931 33.1647C478.293 32.7466 478.709 32.4313 479.179 32.2187C479.649 32.0064 480.132 31.9004 480.629 31.9004C481.176 31.9004 481.661 32.0191 482.085 32.2568C482.508 32.4946 482.855 32.7975 483.125 33.1647ZM482.988 38.8591C482.896 38.967 482.784 39.0746 482.652 39.1823C482.52 39.2904 482.371 39.3883 482.205 39.4771C482.04 39.566 481.858 39.6388 481.662 39.6958C481.466 39.7529 481.257 39.7813 481.036 39.7813C480.698 39.7813 480.402 39.7134 480.147 39.5769C479.892 39.4405 479.679 39.2442 479.507 38.9875C479.335 38.7306 479.206 38.4204 479.12 38.0556C479.034 37.6913 478.991 37.2775 478.991 36.8148C478.991 36.3967 479.023 36.0291 479.087 35.7119C479.152 35.3949 479.237 35.1226 479.341 34.8943C479.445 34.6661 479.566 34.4776 479.705 34.3285C479.843 34.1798 479.989 34.0608 480.142 33.9721C480.296 33.8834 480.448 33.8215 480.598 33.7866C480.748 33.752 480.891 33.7344 481.026 33.7344C481.413 33.7406 481.779 33.839 482.122 34.0292C482.466 34.2193 482.755 34.457 482.988 34.7421V38.8591ZM494.111 33.0035C493.764 32.623 493.352 32.3442 492.876 32.1668C492.4 31.9894 491.88 31.9004 491.315 31.9004C490.775 31.9004 490.251 32.0037 489.745 32.2095C489.238 32.4156 488.789 32.7244 488.396 33.1366C488.003 33.5488 487.689 34.0683 487.456 34.6957C487.223 35.3234 487.106 36.0555 487.106 36.892C487.106 37.7418 487.23 38.4644 487.479 39.0599C487.728 39.6557 488.064 40.1407 488.488 40.5147C488.911 40.889 489.407 41.16 489.975 41.3276C490.543 41.4956 491.143 41.5794 491.776 41.5794C492.402 41.5794 493.007 41.5226 493.59 41.4085C494.174 41.2944 494.606 41.174 494.889 41.0472V39.2501C494.416 39.4153 493.95 39.5416 493.489 39.6303C493.028 39.7196 492.586 39.7637 492.163 39.7637C491.776 39.7637 491.417 39.7206 491.085 39.6352C490.753 39.5497 490.463 39.4134 490.215 39.2263C489.966 39.0397 489.765 38.7954 489.611 38.4942C489.458 38.1934 489.366 37.8302 489.335 37.4056H495.184C495.196 37.1839 495.205 36.9145 495.211 36.5975C495.211 35.7797 495.115 35.0714 494.921 34.4723C494.728 33.8733 494.458 33.3838 494.111 33.0035ZM489.621 34.8723C489.529 35.1133 489.467 35.3702 489.437 35.6425H492.946C492.946 35.3702 492.911 35.1133 492.84 34.8723C492.769 34.6316 492.665 34.4206 492.527 34.24C492.389 34.0593 492.217 33.9168 492.011 33.8121C491.805 33.7077 491.564 33.6553 491.288 33.6553C491.012 33.6553 490.766 33.7077 490.551 33.8121C490.336 33.9168 490.151 34.0593 489.994 34.24C489.837 34.4206 489.713 34.6316 489.621 34.8723ZM504.693 32.9984C504.886 33.2553 505.038 33.5612 505.149 33.916C505.21 34.1127 505.252 34.3266 505.273 34.5578C505.294 34.7893 505.305 35.0571 505.305 35.3613V41.3417H503.122V35.7132C503.122 35.4657 503.113 35.2534 503.095 35.076C503.076 34.8986 503.042 34.7433 502.993 34.61C502.883 34.3187 502.716 34.1094 502.491 33.9826C502.267 33.8557 501.996 33.7924 501.676 33.7924C501.253 33.7924 500.846 33.8955 500.456 34.1016C500.066 34.3074 499.721 34.607 499.42 35V41.3417H497.237V32.1286H499.042L499.245 33.4122H499.281C499.411 33.2221 499.573 33.0352 499.77 32.851C499.966 32.6674 500.19 32.5057 500.442 32.3664C500.694 32.2268 500.972 32.1143 501.276 32.0286C501.58 31.9431 501.903 31.9004 502.247 31.9004C502.573 31.9004 502.887 31.9385 503.191 32.0145C503.495 32.0905 503.773 32.2092 504.025 32.371C504.277 32.5327 504.499 32.7418 504.693 32.9984ZM512.122 39.735C511.674 39.735 511.285 39.6576 510.957 39.5021C510.628 39.3466 510.358 39.1424 510.146 38.8888C509.934 38.6356 509.773 38.3295 509.663 37.9714C509.552 37.6133 509.497 37.2312 509.497 36.8255C509.497 36.2678 509.569 35.7908 509.713 35.3946C509.858 34.9986 510.053 34.68 510.298 34.4391C510.544 34.1981 510.826 34.0223 511.145 33.9114C511.465 33.8006 511.8 33.7451 512.149 33.7451C512.499 33.7451 512.826 33.7816 513.131 33.8544C513.434 33.9271 513.721 34.0239 513.992 34.1443V32.3093C513.691 32.1825 513.356 32.0827 512.988 32.0099C512.619 31.9372 512.236 31.9004 511.836 31.9004C511.302 31.9004 510.76 31.994 510.211 32.1808C509.661 32.368 509.167 32.6595 508.728 33.0557C508.289 33.4519 507.931 33.9669 507.655 34.6008C507.379 35.2345 507.24 35.9952 507.24 36.8826C507.24 37.6753 507.36 38.3673 507.6 38.9602C507.839 39.553 508.167 40.0428 508.585 40.429C509.003 40.8157 509.491 41.104 510.05 41.2944C510.608 41.4845 511.21 41.5794 511.855 41.5794C512.297 41.5794 512.717 41.5367 513.117 41.4512C513.516 41.3655 513.853 41.2625 514.13 41.1421V39.3071C513.841 39.4275 513.534 39.5292 513.209 39.6114C512.883 39.6939 512.521 39.735 512.122 39.735ZM521.916 32.2474C522.472 32.4789 522.943 32.8069 523.33 33.2315C523.717 33.6561 524.013 34.1667 524.218 34.7623C524.424 35.3581 524.527 36.0174 524.527 36.74C524.527 37.4627 524.423 38.122 524.214 38.7176C524.005 39.3134 523.706 39.824 523.316 40.2483C522.926 40.6732 522.455 41.0012 521.902 41.2325C521.349 41.464 520.733 41.5794 520.051 41.5794C519.351 41.5794 518.723 41.464 518.167 41.2325C517.612 41.0012 517.14 40.6732 516.754 40.2483C516.367 39.824 516.071 39.3134 515.865 38.7176C515.659 38.122 515.556 37.4627 515.556 36.74C515.556 36.0174 515.661 35.3581 515.869 34.7623C516.078 34.1667 516.378 33.6561 516.767 33.2315C517.157 32.8069 517.629 32.4789 518.181 32.2474C518.734 32.0161 519.351 31.9004 520.032 31.9004C520.733 31.9004 521.36 32.0161 521.916 32.2474ZM522.103 38.034C521.992 38.4083 521.839 38.7202 521.642 38.9706C521.446 39.2213 521.212 39.4114 520.942 39.541C520.672 39.6711 520.374 39.736 520.049 39.736C519.723 39.736 519.424 39.6711 519.151 39.541C518.877 39.4114 518.641 39.2213 518.441 38.9706C518.242 38.7202 518.087 38.4083 517.976 38.034C517.866 37.66 517.811 37.2292 517.811 36.741C517.811 36.2529 517.866 35.822 517.976 35.4477C518.087 35.074 518.241 34.7603 518.437 34.5066C518.634 34.2532 518.868 34.0631 519.142 33.9362C519.415 33.8094 519.711 33.7461 520.03 33.7461C520.356 33.7461 520.657 33.8094 520.933 33.9362C521.209 34.0631 521.446 34.2532 521.642 34.5066C521.839 34.7603 521.992 35.074 522.103 35.4477C522.213 35.822 522.269 36.2529 522.269 36.741C522.269 37.2292 522.213 37.66 522.103 38.034ZM530.87 35.704C530.49 36.7879 530.17 37.7577 529.912 38.6134H529.875C529.581 37.6247 529.255 36.6356 528.899 35.6469L527.656 32.1289H525.353L528.816 41.3422H530.981L534.435 32.1289H532.114L530.87 35.704ZM543.079 34.4723C543.273 35.0714 543.369 35.7797 543.369 36.5975C543.363 36.9145 543.354 37.1839 543.342 37.4056H537.493C537.524 37.8302 537.616 38.1934 537.77 38.4942C537.923 38.7954 538.124 39.0397 538.373 39.2263C538.622 39.4134 538.912 39.5497 539.243 39.6352C539.575 39.7206 539.934 39.7637 540.321 39.7637C540.744 39.7637 541.187 39.7196 541.647 39.6303C542.108 39.5416 542.574 39.4153 543.047 39.2501V41.0472C542.765 41.174 542.332 41.2944 541.748 41.4085C541.165 41.5226 540.56 41.5794 539.934 41.5794C539.302 41.5794 538.701 41.4956 538.133 41.3276C537.566 41.16 537.07 40.889 536.646 40.5147C536.222 40.1407 535.886 39.6557 535.637 39.0599C535.389 38.4644 535.264 37.7418 535.264 36.892C535.264 36.0555 535.381 35.3234 535.614 34.6957C535.848 34.0683 536.161 33.5488 536.554 33.1366C536.947 32.7244 537.397 32.4156 537.903 32.2095C538.41 32.0037 538.933 31.9004 539.474 31.9004C540.038 31.9004 540.559 31.9894 541.035 32.1668C541.51 32.3442 541.922 32.623 542.269 33.0035C542.616 33.3838 542.886 33.8733 543.079 34.4723ZM537.596 35.6425C537.627 35.3702 537.689 35.1133 537.78 34.8723C537.873 34.6316 537.997 34.4206 538.154 34.24C538.31 34.0593 538.496 33.9168 538.711 33.8121C538.926 33.7077 539.171 33.6553 539.447 33.6553C539.724 33.6553 539.965 33.7077 540.171 33.8121C540.376 33.9168 540.548 34.0593 540.686 34.24C540.824 34.4206 540.929 34.6316 540.999 34.8723C541.07 35.1133 541.106 35.3702 541.106 35.6425H537.596Z"
        fill="black"
      />
      <g>
        <text
          fill="black"
          style={{ whiteSpace: 'pre' }}
          fontFamily="Inter"
          fontSize="24"
          fontWeight="bold"
          letterSpacing="-0.03em"
        >
          <tspan x="0" y="24.7273">
            Low-Pass Sequencing
          </tspan>
        </text>
        <g>
          <text
            fill="black"
            style={{ whiteSpace: 'pre' }}
            fontFamily="Inter"
            fontSize="18"
            fontWeight="300"
            letterSpacing="-0.03em"
          >
            <tspan x="20" y="67.5455">
              Measured
            </tspan>
          </text>
          <rect x="2" y="57" width="10" height="10" fill="#0044FF" />
        </g>
        <g>
          <text
            fill="black"
            style={{ whiteSpace: 'pre' }}
            fontFamily="Inter"
            fontSize="18"
            fontWeight="300"
            letterSpacing="-0.03em"
          >
            <tspan x="150" y="67.5455">
              Imputed
            </tspan>
          </text>
          <rect x="132" y="57" width="10" height="10" fill="#1BECEC" />
        </g>
      </g>
    </g>
  )
}

function Stripe1() {
  return (
    <g id="text-stripe-1" className="v-hidden">
      <g>
        <text
          fill="black"
          style={{ whiteSpace: 'pre' }}
          fontFamily="Inter"
          fontSize="24"
          fontWeight="bold"
          letterSpacing="-0.03em"
        >
          <tspan x="0" y="24.7273">
            Genotyping Arrays
          </tspan>
        </text>
        <g>
          <text
            fill="black"
            style={{ whiteSpace: 'pre' }}
            fontFamily="Inter"
            fontSize="18"
            fontWeight="300"
            letterSpacing="-0.03em"
          >
            <tspan x="20" y="67.5455">
              Measured
            </tspan>
          </text>
          <rect x="2" y="57" width="10" height="10" fill="#0044FF" />
        </g>
      </g>
    </g>
  )
}

function Figure() {
  return (
    <svg
      width="544"
      height="491"
      viewBox="0 0 544 491"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g id="stripes-active">
        <g id="stripe-1-active">
          <g id="stripe-1--active" className="active">
            <rect y="147" width="543" height="32" fill="#F2F5F7" />
            <g>
              <rect x="53.783" y="147" width="4.13714" height="32" fill="#0044FF" />
              <rect id="Rectangle 173" y="147" width="4.13714" height="32" fill="#0044FF" />
              <rect x="182.034" y="147" width="4.13714" height="32" fill="#0044FF" />
              <rect x="364.069" y="147" width="4.13714" height="32" fill="#0044FF" />
              <rect x="538.863" y="147" width="4.13714" height="32" fill="#0044FF" />
              <rect x="235.817" y="147" width="4.13714" height="32" fill="#0044FF" />
              <rect x="417.852" y="147" width="4.13714" height="32" fill="#0044FF" />
            </g>
          </g>

          <g id="stripe-1-inactive" className="inactive">
            <rect opacity="0.05" x="1" y="147" width="543" height="32" fill="#666666" />
            <g>
              <rect opacity="0.05" x="54.783" y="147" width="4.13714" height="32" fill="#666666" />
              <rect opacity="0.05" x="1" y="147" width="4.13714" height="32" fill="#666666" />
              <rect opacity="0.05" x="183.034" y="147" width="4.13714" height="32" fill="#666666" />
              <rect opacity="0.05" x="365.069" y="147" width="4.13714" height="32" fill="#666666" />
              <rect opacity="0.05" x="539.863" y="147" width="4.13714" height="32" fill="#666666" />
              <rect opacity="0.05" x="236.817" y="147" width="4.13714" height="32" fill="#666666" />
              <rect opacity="0.05" x="418.852" y="147" width="4.13714" height="32" fill="#666666" />
            </g>
          </g>

          <Stripe1 />
        </g>
        <g id="stripe-2-active">
          <g id="stripe-2-inactive" className="inactive">
            <rect opacity="0.05" x="1" y="251" width="543" height="32" fill="#666666" />
            <rect opacity="0.05" x="30.5165" y="251" width="17.2187" height="32" fill="#666666" />
            <rect opacity="0.05" x="112.513" y="251" width="30.3377" height="32" fill="#666666" />
            <rect opacity="0.05" x="240.421" y="251" width="39.357" height="32" fill="#666666" />
            <rect opacity="0.05" x="341.276" y="251" width="22.9582" height="32" fill="#666666" />
            <rect opacity="0.05" x="439.667" y="251" width="62.3152" height="32" fill="#666666" />
          </g>

          <g id="stripe-2--active" className="active">
            <rect y="251" width="543" height="32" fill="#0044FF" />
            <rect x="29.5166" y="251" width="17.2187" height="32" fill="#1BECEC" />
            <rect x="111.513" y="251" width="30.3377" height="32" fill="#1BECEC" />
            <rect x="239.421" y="251" width="39.357" height="32" fill="#1BECEC" />
            <rect x="340.276" y="251" width="22.9582" height="32" fill="#1BECEC" />
            <rect x="438.667" y="251" width="62.3152" height="32" fill="#1BECEC" />
          </g>

          <Stripe2 />
        </g>
        <g id="stripe-3-active">
          <g id="stripe-3-inactive" className="inactive">
            <rect opacity="0.05" x="1" y="355.001" width="543" height="32" fill="#666666" />
            <rect opacity="0.05" x="31.5488" y="355" width="17.2187" height="32" fill="#666666" />
            <rect
              opacity="0.05"
              x="56"
              y="355"
              width="32"
              height="8"
              transform="rotate(90 56 355)"
              fill="#666666"
            />
            <rect
              opacity="0.05"
              x="441.297"
              y="355"
              width="32"
              height="32"
              transform="rotate(90 441.297 355)"
              fill="#666666"
            />
            <rect
              opacity="0.05"
              x="163"
              y="355"
              width="32"
              height="22"
              transform="rotate(90 163 355)"
              fill="#666666"
            />
            <rect opacity="0.05" x="112.513" y="355" width="30.3377" height="32" fill="#666666" />
            <rect opacity="0.05" x="240.421" y="355" width="39.357" height="32" fill="#666666" />
            <rect opacity="0.05" x="341.615" y="355" width="22.9114" height="32" fill="#666666" />
            <rect
              opacity="0.05"
              x="349.554"
              y="355"
              width="32"
              height="32"
              transform="rotate(90 349.554 355)"
              fill="#666666"
            />
            <rect opacity="0.05" x="439.667" y="355" width="62.3152" height="32" fill="#666666" />
          </g>

          <g id="stripe-3--active" className="active">
            <rect y="355.001" width="543" height="32" fill="#0044FF" />
            <rect x="30.5488" y="355" width="17.2187" height="32" fill="#1BECEC" />
            <rect x="55" y="355" width="32" height="8" transform="rotate(90 55 355)" fill="black" />
            <rect
              x="440.297"
              y="355"
              width="32"
              height="32"
              transform="rotate(90 440.297 355)"
              fill="black"
            />
            <rect
              x="162"
              y="355"
              width="32"
              height="22"
              transform="rotate(90 162 355)"
              fill="black"
            />
            <rect x="111.513" y="355" width="30.3377" height="32" fill="#1BECEC" />
            <rect x="239.421" y="355" width="39.357" height="32" fill="#1BECEC" />
            <rect x="340.615" y="355" width="22.9114" height="32" fill="#1BECEC" />
            <rect
              x="341"
              y="355"
              width="32"
              height="11"
              transform="rotate(90 341 355)"
              fill="black"
            />
            <rect x="438.667" y="355" width="62.3152" height="32" fill="#1BECEC" />
          </g>

          <Stripe3 />
        </g>
        <g id="stripe-4-active">
          <g id="stripe-4-inactive" className="inactive">
            <rect opacity="0.05" x="1" y="459.001" width="543" height="32" fill="#666666" />
          </g>

          <g id="stripe-4--active" className="active">
            <rect y="459.001" width="543" height="32" fill="#0044FF" />
          </g>

          <Stripe4 />
        </g>
      </g>

      <style jsx>{`
        svg {
          width: 100%;
          height: auto;
        }

        @media (max-width: 992px) {
          svg {
            margin-top: 48px;
          }
        }
      `}</style>

      <style global jsx>{`
        #stripe-1-active,
        #stripe-2-active,
        #stripe-3-active,
        #stripe-4-active {
          cursor: pointer;
        }

        .inactive,
        .v-hidden {
          opacity: 0;
          transition: opacity 0.35s ease;
        }

        #stripe-1-active:hover #text-stripe-1,
        #stripe-2-active:hover #text-stripe-2,
        #stripe-3-active:hover #text-stripe-3,
        #stripe-4-active:hover #text-stripe-4 {
          opacity: 1 !important;
        }

         {
          /* #stripes-active:hover .inactive {
          opacity: 1 !important;
        }
        #stripes-active:hover .active {
          opacity: 0 !important;
        } */
        }
      `}</style>
    </svg>
  )
}

function Sequencing() {
  return (
    <div className={styles.wrap}>
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <RevealText>
              <h2 className={styles.title}>
                What is Low-Pass <br /> Sequencing?
              </h2>
            </RevealText>
            <div className="pr-md-12">
              <p className={styles.subtitle}>
                Gencove’s imputation and analysis platform returns over 99% accurate variant calls
                for every sample.
              </p>
              <p className={styles.copy}>
                The components of a low-pass sequencing assay involve multiplexing large numbers of
                DNA samples in a single lane or run of a sequencer, sequencing them at a very low
                coverage (frequently starting at 0.4x), then performing genotype imputation to make
                genotype calls at all sites known to be polymorphic in the population.
              </p>
            </div>
          </div>
          <div className="col-md-6">
            <Figure />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Sequencing
