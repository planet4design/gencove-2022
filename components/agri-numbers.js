import styles from 'styles/features.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'

function Numbers(props) {
  return (
    <div className={styles.wrap}>
      <div className={cn('container', styles.content)}>
      
      <div className="row">
        <div className="col-lg">
          <h2 className={styles.title}>Complete, cost-effective and concordant</h2>
        </div>
      </div>


        <div className="row">
          <div className="col-lg">
            <RevealText>
            <p className={styles.paragraph}>
                <img alt="timer" src="/img/10x-1.png" />
            </p>
            </RevealText>
          </div>


          <div className="col-lg">
            <RevealText>
            <p className={styles.paragraph}>
                <img alt="timer" src="/img/10x-2.png" />
            </p>
            </RevealText>
          </div>
        </div>


        <div className="row">
          <div className="col-lg">
            <RevealText>
            <p className={styles.paragraph}>
                <img alt="timer" src="/img/10x-3.png" />
            </p>
            </RevealText>
          </div>


          <div className="col-lg">
            <RevealText>
            <p className={styles.paragraph}>
                <img alt="timer" src="/img/99percent.png" />
            </p>
            </RevealText>
          </div>


        </div>

      </div>
    </div>
  )
}

export default Numbers

