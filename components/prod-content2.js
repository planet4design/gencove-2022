import styles from 'styles/about-prod.module.scss'

function Focus() {
  return (
    <div className={styles.wrapWhite}>
      <div className="container">
        <div className="row">
          <div className="col-lg-1 col-2">
            <p className={styles.rowNum}>
              1 
            </p>
          </div>
          <div className="col-lg-11 col-10">
            <p className={styles.smallTitle}>
              Library preparation
            </p>
            <p className={styles.subTitle}>
              The first step is to prepare your population’s DNA samples. For the library preparation, we provide the necessary protocols and consultative services depending on your sequencing hardware.
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-1 col-2">
            <p className={styles.rowNum}>
              2
            </p>
          </div>
          <div className="col-lg-11 col-10">
            <p className={styles.smallTitle}>
              Low-pass whole genome sequencing
            </p>
            <p className={styles.subTitle}>
              Next, the genome is randomly sequenced but at shallower depths hence the name low-coverage or low-pass sequencing. We have three options for sequencing your samples: in our lab, our preferred network of service providers, or you may directly upload a FastQ file to the Gencove platform.  Our lightning-fast speeds and multiple upload paths ensure this step is simple.
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-1 col-2">
            <p className={styles.rowNum}>
              3
            </p>
          </div>
          <div className="col-lg-11 col-10">
            <p className={styles.smallTitle}>
              Imputation and anlysis
            </p>
            <p className={styles.subTitle}>
            Once the low-pass data is uploaded to the Gencove platform, it is compared to a fully sequenced and assembled standard haplotype genome of that particular organism. This step, called imputation, results in millions of over 99% accurate variant calls for every sample. Gencove builds comprehensive reference panels and continually enhances our data sets and algorithms to ensure the highest performing imputation.  To further extend the value of your investment, we offer additional analysis, without the hassle of designing and developing another array. Now, your team can focus on innovation instead of struggling with the mechanics of large-scale genomic data analysis.
            </p>
              <h3>
                <a href="/contact-us" className={styles.btnBlue}>
                  Ask a question <span className={styles.outlineArrowBlue}>&gt;</span>
                </a>
              </h3>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Focus
