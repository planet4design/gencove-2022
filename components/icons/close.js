import * as React from 'react'

function Close(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 24 24" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M11.485 10.485L18.971 3l1.414 1.414-7.486 7.486 8.072 8.07-1.415 1.415-8.07-8.071-8.072 8.07L2 19.972l8.071-8.072-7.485-7.485L4 3l7.485 7.485z"
        fill="#fff"
      />
    </svg>
  )
}

const MemoClose = React.memo(Close)
export default MemoClose
