import * as React from 'react'

function Chevron(props) {
  const { className, onClick } = props
  return (
    <svg
      width="26"
      height="19"
      viewBox="0 0 26 19"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      onClick={onClick}
    >
      <path d="M14 1L24 9.5L14 18" stroke="currentColor" strokeWidth="2" />
      <line x1="24" y1="9.5" x2="-8.74231e-08" y2="9.5" stroke="currentColor" strokeWidth="2" />
    </svg>
  )
}

const MemoChevron = React.memo(Chevron)
export default MemoChevron
