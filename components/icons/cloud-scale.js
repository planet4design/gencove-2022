import * as React from 'react'

function CloudScale(props) {
  return (    
    <svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path fillRule="evenodd" clipRule="evenodd" d="M64.1477 19.0706L64.1477 18.0706L63.1477 18.0706L46.4128 18.0706L46.4128 20.0706L60.7337 20.0706L42.5046 38.2997L43.9188 39.7139L62.1477 21.485L62.1477 35.8054L64.1477 35.8054L64.1477 19.0706ZM62.1477 20.2241L62.1477 20.0706L61.9942 20.0706L62.1477 20.2241Z" fill="#0044FF"/>
      <path d="M20.6406 8.4375H6.21875V70.5625H20.6406" stroke="#0044FF" strokeWidth="2"/>
      <path d="M58.3594 70.5625H72.7812V8.4375H58.3594" stroke="#0044FF" strokeWidth="2"/>
    </svg>
  )
}

const MemoCloudScale = React.memo(CloudScale)
export default MemoCloudScale
