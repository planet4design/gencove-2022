import * as React from 'react'

function Support(props) {
  return (  
    <svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path d="M6.48633 11.3823H71.9145V55.5413H19.2692L6.48633 66.0001V55.5413V11.3823Z" stroke="#0044FF" strokeWidth="2"/>
      <line x1="23" y1="29" x2="57" y2="29" stroke="#0044FF" strokeWidth="2"/>
      <line x1="23" y1="38" x2="57" y2="38" stroke="#0044FF" strokeWidth="2"/>
    </svg>
  )
}

const MemoSupport = React.memo(Support)
export default MemoSupport
