import * as React from 'react'

function Linkedin(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 20 20" fill="none" {...props}>
      <path
        d="M17.038 17.044h-2.959v-4.642c0-1.107-.023-2.53-1.546-2.53-1.543 0-1.778 1.201-1.778 2.448v4.723H7.793V7.5h2.845v1.302h.038c.398-.752 1.365-1.542 2.807-1.542 3 0 3.558 1.975 3.558 4.544v5.24h-.003zM4.447 6.194a1.719 1.719 0 01-1.429-2.677 1.72 1.72 0 111.429 2.678zm1.484 10.85H2.962V7.5h2.969v9.544zM18.522 0H1.476C.661 0 0 .644 0 1.442v17.116C0 19.356.66 20 1.475 20h17.044c.814 0 1.481-.644 1.481-1.442V1.442C20 .644 19.332 0 18.52 0h.003z"
        fill="currentColor"
      />
    </svg>
  )
}

const MemoLinkedin = React.memo(Linkedin)
export default MemoLinkedin
