import * as React from 'react'

function Compliance(props) {
  return (      
    <svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path d="M10 5H67V70.3125H10V5Z" stroke="#0044FF" strokeWidth="2"/>
      <line x1="19" y1="26" x2="57" y2="26" stroke="#0044FF" strokeWidth="2"/>
      <line x1="19" y1="34" x2="57" y2="34" stroke="#0044FF" strokeWidth="2"/>
      <line x1="19" y1="43" x2="36.8125" y2="43" stroke="#0044FF" strokeWidth="2"/>
    </svg>
  )
}

const MemoCompliance = React.memo(Compliance)
export default MemoCompliance
