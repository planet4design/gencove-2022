import * as React from 'react'

function Menu(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 24 24" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M22 5H2V3h20v2zm0 8H2v-2h20v2zM2 21h20v-2H2v2z"
        fill="currentColor"
      />
    </svg>
  )
}

const MemoMenu = React.memo(Menu)
export default MemoMenu
