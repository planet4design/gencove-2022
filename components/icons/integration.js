import * as React from 'react'

function Integration(props) {
  return (  
    <svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path d="M59.1376 48.4694C53.4683 48.4694 48.8724 53.0653 48.8724 58.7346C48.8724 64.4039 53.4683 68.9998 59.1376 68.9998C64.807 68.9998 69.4028 64.4039 69.4028 58.7346C69.4028 53.0653 64.807 48.4694 59.1376 48.4694ZM59.1376 48.4694L59.1376 16.3906H37.3241" stroke="#0044FF" strokeWidth="2"/>
      <path d="M18.0757 27.9403C23.7451 27.9403 28.3409 23.3444 28.3409 17.6751C28.3409 12.0058 23.7451 7.40991 18.0757 7.40991C12.4064 7.40991 7.81055 12.0058 7.81055 17.6751C7.81055 23.3444 12.4064 27.9403 18.0757 27.9403Z" stroke="#0044FF" strokeWidth="2"/>
      <path d="M18.0767 27.9373V68.9981" stroke="#0044FF" strokeWidth="2"/>
    </svg>
  )
}

const MemoIntegration = React.memo(Integration)
export default MemoIntegration
