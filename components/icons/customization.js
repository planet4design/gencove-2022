import * as React from 'react'

function Customization(props) {
  return (  
    <svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path d="M12.6694 67.0998V44.9331" stroke="#0044FF" strokeWidth="2"/>
      <path d="M12.6694 32.2636V10.0969" stroke="#0044FF" strokeWidth="2"/>
      <path d="M63.3349 67.0972V51.2639" stroke="#0044FF" strokeWidth="2"/>
      <path d="M63.3354 38.5969V10.0969" stroke="#0044FF" strokeWidth="2"/>
      <path d="M3.16943 44.9331H22.1694" stroke="#0044FF" strokeWidth="2"/>
      <path fillRule="evenodd" clipRule="evenodd" d="M37.0012 24.9329H28.5012V26.9329H47.5012V24.9329H39.0012V10.0969H37.0012V24.9329ZM39.0012 38.5969V67.0969H37.0012V38.5969H39.0012Z" fill="#0044FF"/>
      <path d="M53.8349 51.2639H72.8349" stroke="#0044FF" strokeWidth="2"/>
    </svg>
  )
}

const MemoCustomization = React.memo(Customization)
export default MemoCustomization
