import * as React from 'react'

function Security(props) {
  return (    
    <svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <rect x="9" y="34" width="61" height="43" stroke="#0044FF" strokeWidth="2"/>
      <path d="M22.5 27.25V18.9167C22.5 14.4964 24.2559 10.2572 27.3816 7.13155C30.5072 4.00595 34.7464 2.25 39.1667 2.25C43.5869 2.25 47.8262 4.00595 50.9518 7.13155C54.0774 10.2572 55.8333 14.4964 55.8333 18.9167V27.25" stroke="#0044FF" strokeWidth="2" strokeLinejoin="round"/>
    </svg>
  )
}

const MemoSecurity = React.memo(Security)
export default MemoSecurity
