import * as React from 'react'

function Arrow(props) {
  return (
    <svg
      width="16"
      height="16"
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16ZM5.88704 4.61296L9.27408 8L5.88704 11.387L6.9477 12.4477L10.8651 8.53033L11.3954 8L10.8651 7.46967L6.9477 3.5523L5.88704 4.61296Z"
        fill="currentColor"
      />
    </svg>
  )
}

const MemoArrow = React.memo(Arrow)
export default MemoArrow
