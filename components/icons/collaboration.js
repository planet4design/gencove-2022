import * as React from 'react'

function Collaboration(props) {
  return (  
    <svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <circle cx="40" cy="18.25" r="12.5" stroke="#0044FF" strokeWidth="2"/>
      <circle cx="15" cy="58.25" r="12.5" stroke="#0044FF" strokeWidth="2"/>
      <circle cx="66.25" cy="58.25" r="12.5" stroke="#0044FF" strokeWidth="2"/>
    </svg>
  )
}

const MemoCollaboration = React.memo(Collaboration)
export default MemoCollaboration
