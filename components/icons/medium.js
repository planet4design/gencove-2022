import * as React from 'react'

function Medium(props) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 30 18" fill="none" {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M16.888 8.502c0 4.695-3.78 8.502-8.444 8.502C3.781 17.004 0 13.197 0 8.502S3.78 0 8.444 0c4.663 0 8.444 3.807 8.444 8.502zm9.26.002c0 4.42-1.891 8.004-4.223 8.004s-4.222-3.584-4.222-8.004S19.593.5 21.925.5c2.332 0 4.222 3.583 4.222 8.004zm2.305 7.165c.82 0 1.485-3.212 1.485-7.17 0-3.96-.665-7.17-1.485-7.17-.82 0-1.484 3.21-1.484 7.17s.664 7.17 1.484 7.17z"
        fill="currentColor"
      />
    </svg>
  )
}

const MemoMedium = React.memo(Medium)
export default MemoMedium
