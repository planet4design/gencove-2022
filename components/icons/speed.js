import * as React from 'react'

function Speed(props) {
  return (  
    <svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path d="M33.6111 12.5L3.75 48.5H30.625L27.6389 72.5L36.4521 61.875L46.976 49.1875L57.5 36.5H30.625L33.6111 12.5Z" stroke="#0044FF" strokeWidth="2"/>
      <line x1="48.75" y1="11.5" x2="72.5" y2="11.5" stroke="#0044FF" strokeWidth="2"/>
      <line x1="48.75" y1="20.25" x2="65" y2="20.25" stroke="#0044FF" strokeWidth="2"/>
    </svg>

  )
}

const MemoSpeed = React.memo(Speed)
export default MemoSpeed
