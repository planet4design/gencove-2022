import styles from 'styles/text.module.scss'

export function H1({ children, ...rest }) {
  return (
    <h1 className={styles.h1} {...rest}>
      {children}
    </h1>
  )
}

export function H2({ children, ...rest }) {
  return (
    <h2 className={styles.h2} {...rest}>
      {children}
    </h2>
  )
}
