import styles from 'styles/about-company.module.scss'
import RevealText from 'components/reveal-text'
import HorizontalLines from 'components/horizontal-lines'

function Focus() {
  return (
    <div className={styles.wrapQuote}>
      <div className="container">

        <div className="row">
          <div className="col-lg-1">
            <RevealText>
              <img src="/img/testimonials/quotes.png" alt="quotes" />
            </RevealText>
          </div>
          <div className="col-lg-11">
            <RevealText>
              <h4 className={styles.quote}>
                We are committed to making genomic information ubiquitous.
              </h4>
            </RevealText>
            <p className={styles.quoter}>
              <span className={styles.bold}>Tomaz Berisa</span><br />
              Co-founder and CTO
            </p>
          </div>
        </div>

      </div>
    </div>
  )
}

export default Focus
