import RevealText from 'components/reveal-text'
import styles from 'styles/prefooter.module.scss'
import cn from 'classnames'

function PreFooter({ variant }) {
  return (
    <div
      className={cn(
        styles.wrap,
        variant === 'index' ? styles.index : variant === 'about' && styles.about
      )}
    >
      <div className={cn('container', styles.container)}>
          <a href="/contact-us" className={styles.btnBlue}><h3 className={styles.title}><strong>START LEVERAGING</strong> the power of sequencing <span className={styles.outlineArrowBlue} >&gt;</span></h3></a>
      </div>
    </div>
  )
}

export default PreFooter
