import styles from 'styles/press.module.scss'
import cn from 'classnames'

function CompanyContentTwo() {
  return (
    <div className={styles.wrapSmaller}>
      <div className={cn('container', styles.content)}>
        
        <div className="row">

          <div className="col-lg">

            <p className={styles.subTitle}>March 13, 2019 - Gencove</p>

            <h2 className={styles.titleSmaller}>Gencove raises $3M investment led by Spero Ventures to expand genome sequencing platform</h2>
 
            <p className={styles.subTitle}>
<strong>NEW YORK.</strong> – March 13, 2019 /PRNewswire/ -- Gencove, the leading low-pass genome sequencing platform, announced today a $3 million investment led by Spero Ventures. Alexandria Venture Investments and Burst Capital participated in the round, along with existing investors Third Kind Venture Capital and Version One Ventures. The funding will be used to develop new applications for agricultural markets as well as expand Gencove&rsquo;s commercial operations in human genetics. Shripriya Mahesh, partner at Spero, will join Gencove&rsquo;s board of directors.</p>

<p className={styles.subTitle}>&quot;Genomics will be foundational to the health and well-being of humanity and the planet,&quot; said Shripriya Mahesh. &quot;We are excited partner with Gencove in its mission to bring affordable whole genome sequencing to customers and industries that have never before been able to affordably integrate it at scale.&quot; </p>

<p className={styles.subTitle}>&quot;With their operational expertise and experience in scaling software companies, Spero is the ideal partner as we expand Gencove into new markets,&quot; said Joe Pickrell, CEO of Gencove. &quot;We&rsquo;re excited to have a partner on board that brings a deep understanding of opportunities in both human and agricultural genomics.&quot;</p>

<p className={styles.subTitle}>Founded by leading scientists from the New York Genome Center, Gencove develops technologies that enable high-throughput and low-cost genome sequencing. Using its innovative genome imputation technology, Gencove delivers high quality genomic interpretation from small amounts of sequencing data. Gencove&rsquo;s technology partners include BGI and SeqWell. Customers span consumer, clinical, and agricultural markets and include leading academic institutions like the Broad Institute, University of Michigan, and Oxford University.</p>


<p className={styles.subTitle}><strong>About Gencove</strong></p>

<p className={styles.subTitle}>With additional threats to human health and the challenge of feeding the world&rsquo;s growing population, demand for genomic information has skyrocketed. Historically, DNA processing options fell into two categories: low-throughput and costly options such as whole genome sequencing, and high-throughput but limited technologies like genotyping arrays. To help scientists make life-saving discoveries, Gencove combines low-pass whole genome sequencing (reading the DNA at shallow depths) with a proprietary software-as-a-service (SaaS) computation layer. The result is a fast, high-volume, and cost-sensitive sequencing solution. As evidence of the technology&rsquo;s value, the company has hundreds of customers, and the largest genomics service providers in the world partner with Gencove.&nbsp; For more information visit: www.gencove.com and follow us on www.linkedin.com/company/gencove/mycompany/ and twitter.com/Gencove.</p>

<p className={styles.subTitle}><strong>About Spero Ventures</strong></p>

<p className={styles.subTitle}>Spero Ventures is an early-stage venture capital firm that invests in the things that make life worth living: well-being, work & purpose, and human connection. Spero is a diverse team of thesis-driven, high-conviction investors that fund purpose-driven companies that can reach venture scale.  Spero&rsquo;s sole LP is Pierre Omidyar, an entrepreneur, and philanthropist best known as the founder of eBay. Learn more at www.spero.vc.</p>

<p className={styles.subTitle}>Contacts</p>
<p className={styles.subTitle}>For Gencove:</p>
<p className={styles.subTitle}>Kristi Ashton, 650.224.8231</p>
<p className={styles.subTitle}>kristi.ashton@gencove.com</p>


      </div>
    </div>
      </div>
    </div>
  )
}

export default CompanyContentTwo

