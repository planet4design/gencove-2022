import styles from 'styles/about-focus.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'

function Focus() {
  return (
    <div className={styles.wrap}>
      <div className="container">
        <RevealText>
          <h2 className={styles.title}>
           CONTENT 1
          </h2>
        </RevealText>
        <p className={styles.subTitle}>
          SUB CONTENT 1
        </p>
      </div>
    </div>
  )
}

export default Focus
