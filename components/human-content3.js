import styles from 'styles/about-human.module.scss'

function AgriContentThree() {
  return (
    <div className={styles.wrapSmallerWhite}>
      <div className="container">
        <div className="row">
          <div className="col-lg-1 col-2">
            <p className={styles.rowNum}>
              1 
            </p>
          </div>
          <div className="col-lg-11 col-10">
            <p className={styles.smallTitle}>
              Library preparation
            </p>
            <p className={styles.subTitle}>
              Our low-pass sequencing library preparation method is optimized for output and cost-efficiency. It&rsquo;s also automated, scalable, and battle-tested.
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-1 col-2">
            <p className={styles.rowNum}>
              2
            </p>
          </div>
          <div className="col-lg-11 col-10">
            <p className={styles.smallTitle}>
              Low-Pass Whole Genome Sequencing
            </p>
            <p className={styles.subTitle}>
             We sequence a random 10-20% of the genome but at shallower depths (&lt;1x) hence the name low-coverage or low-pass sequencing. We have three options for sequencing your samples: in our lab, our preferred network of service providers, or you may directly upload a FastQ file to the Gencove platform.
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-1 col-2">
            <p className={styles.rowNum}>
              3
            </p>
          </div>
          <div className="col-lg-11 col-10">
            <p className={styles.smallTitle}>
              Imputation and analysis
            </p>
            <p className={styles.subTitle}>
            Using our proprietary algorithms, we compare the sequence to reference panels to complete the genome. Gencove’s products return over 99% accurate variant calls for every sample. We then provide imputed VCF and aligned BAM files plus any additional analysis that you need to complete your project.
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AgriContentThree
