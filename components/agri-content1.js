import styles from 'styles/black-section.module.scss'

function AgriContent() {
  return (
    <div className={styles.wrap}>
      <div className="container">
        <div className="row">
          <div className="col-lg">
            <p className={styles.subTitle}>
              Genomics is critical for diverse and sustainable plant and animal breeding, especially as the climate and consumer preferences change. However, acquiring genomic information is challenging in one way or another. Whole Genome Sequencing (WGS), while complete, is costly and intensive, therefore entire populations are not typically sequenced leading to selection bias. Genotyping arrays are less expensive but only provide a tiny fraction of information hindering breakthrough discovery and predictive power.
            </p>
          </div>
          <div className="col-lg">
          <p className={styles.subTitle}>
            We recognize that the universal application of genomic information will lead to a healthier and more sustainable world, which is why we developed a high-volume, cost-sensitive genome sequencing software products so you get the insights you need to quickly go from DNA to trait selection.
          </p>
          <h3>
            <a href="http://gencove-6646953.hs-sites.com/the-agrigenomics-brocure/" className={styles.btnBlue} target="_blank">
           Agrigenomics brochure <span className={styles.outlineArrowBlue} >&gt;</span></a>
          </h3>
        </div>
        </div>
      </div>
    </div>
  )
}

export default AgriContent
