import styles from 'styles/about-hero.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'
import { useInView } from 'react-hook-inview'

function Hero() {
  const [ref, inView] = useInView({ threshold: 0, unobserveOnEnter: true })

  return (
    <div className={cn(styles.wrap, 'hero')}>
      <div className={cn('container', styles.content)}>
        <RevealText stagger={0.125}>
          <h1 className={styles.title}>
            BANNER CONTENT
          </h1>
        </RevealText>
      </div>
    </div>
  )
}

export default Hero



