import styles from 'styles/lightbox.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'

function Lightbox(props) {
  return (
    <div className={styles.wrap}>
      <div className={cn('container', styles.content)}>
      
        <div className="row">
          <div className="col-lg">
                <img alt="timer" src="/img/infographic1.png" />
          </div>
        </div>


        <div className="row">
          <div className="col-lg">
            <RevealText>
            <p className={styles.paragraph}>
            DNA is a long molecule in the shape of double helix but can be simplified into a sequence of four letters, adenine (A), thymine (T), guanine (G), and cytosine (C), which are the nucleotide bases. A gene is a functional unit of DNA, which can be as short as a few hundred base pairs or as long as many thousands.
            </p>
            </RevealText>
          </div>
        </div>

        <div className="row">
          <div className="col-lg">
            <RevealText>
                <img alt="timer" src="/img/infographic2.png" />
            </RevealText>
          </div>
        </div>


        <div className="row">
          <div className="col-lg">
            <RevealText>
            <p className={styles.paragraph}>
            By reading the nucleotide bases, changes, called variants, can be measured.
            </p>
            </RevealText>
          </div>
        </div>        

        <div className="row">
          <div className="col-lg">
            <RevealText>
                <img alt="timer" src="/img/infographic3.png" />
            </RevealText>
          </div>
        </div>


        <div className="row">
          <div className="col-lg">
            <RevealText>
            <p className={styles.paragraph}>
            Studying the genome and variants is important to understanding how genes influence phenotypes (the visible expression of our genes or physical traits).
            </p>
            </RevealText>

            <p className={styles.paragraph}>&nbsp;
            </p>

            <RevealText>
            <p className={styles.paragraph}>
            How are genomes measured?
            </p>
            </RevealText>

            <p className={styles.paragraph}>&nbsp;
            </p>

            <RevealText>
            <p className={styles.paragraph}>
            One way is called genotyping arrays, which are are typically glass slides that are printed with thousands of tiny spots in defined positions, each spot containing a known part of the DNA or gene.
            </p>
            </RevealText>

          </div>
        </div>   


        <div className="row">
          <div className="col-lg">
            <RevealText>
                <img alt="timer" src="/img/infographic4.png" />
            </RevealText>
          </div>
        </div>


        <div className="row">
          <div className="col-lg">
            <RevealText>
            <p className={styles.paragraph}>
            The other option is called Whole Genome Sequencing (WGS), which is the process of determining the exact order of the nucleotide bases (A, T, G, C). WGS requires substantial human and computational resources to acquire and analyze large and complex amounts of sequence data generated.
            </p>
            </RevealText>
          </div>
        </div>   



        <div className="row">
          <div className="col-lg">
            <RevealText>
                <img alt="timer" src="/img/infographic5.png" />
            </RevealText>
          </div>
        </div>


        <div className="row">
          <div className="col-lg">
            <RevealText>
            <p className={styles.paragraph}>
           Gencove developed a new method called low-pass whole genome sequencing, where the whole genome is read but at shallower depths hence the name low-coverage or low-pass sequencing.
            </p>
            </RevealText>
          </div>
        </div>   



        <div className="row">
          <div className="col-lg">
            <RevealText>
                <img alt="timer" src="/img/infographic6.png" />
            </RevealText>
          </div>
        </div>


        <div className="row">
          <div className="col-lg">
            <RevealText>
            <p className={styles.paragraph}>
            Then the Gencove platform fills in the gaps by comparing the low-pass sequencing reads to a set of reference genomes ina process called imputation.
            </p>
            </RevealText>
          </div>
        </div>   


        <div className="row">
          <div className="col-lg">
            <RevealText>
                <img alt="timer" src="/img/infographic7.png" />
            </RevealText>
          </div>
        </div>


        <div className="row">
          <div className="col-lg">
            <RevealText>
            <p className={styles.paragraph}>
            When high-coverage at regions of interest is required, Gencove can also provide high-coverage sequencing at specific loci.
            </p>
            </RevealText>
          </div>
        </div>       



        <div className="row">
          <div className="col-lg">
            <RevealText>
                <img alt="timer" src="/img/infographic8.png" />
            </RevealText>
          </div>
        </div>


        <div className="row">
          <div className="col-lg">

            <h2 className={styles.titleSmaller}>
              The Gencove software platform:
            </h2>

            <p className={styles.paragraph}>- Delivers high-throughput, cost-effective low-pass whole genome sequencing</p>

            <p className={styles.paragraph}>&nbsp;</p>

            <p className={styles.paragraph}>- Offers high-coverage at targeted regions for even greater discovery</p>

            <p className={styles.paragraph}>&nbsp;</p>

            <p className={styles.paragraph}>- Fills in sequencing gaps by imputation to increase accuracy</p>
            
            <p className={styles.paragraph}>&nbsp;</p>
            
            <p className={styles.paragraph}>- Provides vastly more information than arrays and is more affordable than WGS</p>

            <p className={styles.paragraph}>&nbsp;</p>

            <p className={styles.paragraph}>&nbsp;</p>

            <p className={styles.paragraph}>&nbsp;</p>

            <p className={styles.paragraph}>&nbsp;</p>


          </div>
        </div>   




          </div>
        </div>


       

  )
}

export default Lightbox
      
