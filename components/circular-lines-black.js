import Lottie from 'lottie-react-web'
import animation from 'assets/circular-lines-black.json'

function Shape({ inView }) {
  return (
    <div id="circular-lines-black">
      <Lottie
        isPaused={inView ? false : true}
        options={{
          animationData: animation,
          autoplay: false,
          loop: false
        }}
      />

      <style jsx>{`
        div {
          content: ' ';
          position: absolute;
          width: 728px;
          height: 728px;
          left: -308px;
          bottom: -243px;
          z-index: -1;
        }

        @media (max-width: 768px) {
          div {
            width: 268px;
            height: 268px;
            left: unset;
            right: -111px;
            bottom: -53px;
          }
        }
      `}</style>

      <style global jsx>{`
        @media (max-width: 768px) {
          #circular-lines-black path {
            stroke-width: 4px !important;
          }
        }
      `}</style>
    </div>
  )
}

export default Shape
