import styles from 'styles/enterprise.module.scss'
import cn from 'classnames'
import Arrow from 'components/icons/arrow'
import { useState, useEffect, useRef } from 'react'
import RevealText from 'components/reveal-text'
import { useInView } from 'react-hook-inview'
function Enterprise() {
  const [ended, setEnded] = useState(false)
  const [play, setPlay] = useState(false)
  const [ref, inView] = useInView({ threshold: 0.1, unobserveOnEnter: true })
  // detect if video ended
  let listener = null
  useEffect(() => {
    if (document.getElementById('video') != null) {
      listener = document.getElementById('video').addEventListener('ended', () => {
        console.log('ended')
        setEnded(true)
        setPlay(false)
      })
    }
    return () => {
      document.removeEventListener('ended', listener)
    }
  }, [])
  // play video again
  const handlePlay = () => {
    setPlay(true)
    setEnded(false)
  }
  useEffect(() => {
    if (play) {
      document.getElementById('video').play()
    }
  }, [play])
  // features logos
  const features = [
  ]
  return (
    <div className={styles.wrap} ref={ref}>
      <div className={styles.bgDark}>
        <div className={cn('container', styles.container)}>
          <div className={styles.saas}>
            <div className={styles.saasText}>
              <RevealText stagger={0.1}>
                <h3 className={styles.heading2}>
                  The only enterprise analytics platform for low-pass sequencing
                </h3>
              </RevealText>
        <RevealText>
        <p className={styles.subTitle}>The Gencove products are scalable, secure, and flexible. It&rsquo;s an end-to-end solution that enables your team to focus on breakthrough discoveries instead of the mechanics of large-scale genomic data analysis.
                          <a href="/contact-us" className={styles.btnBlue}>Get a demo <span className={styles.outlineArrowBlue} >&gt;</span></a>
</p>
                   
        </RevealText>

            </div>
            <div className={styles.saasImg}>
              <div className={styles.videoBox}>
                <video
                  id="video"
                  className={styles.video}
                  muted
                  playsInline
                  autoPlay
                  poster="/img/enterprise/dashboard.jpg"
                >
                  <source src="video/saas-video.webm" type="video/webm" />
                  <source src="video/saas-video.mp4" type="video/mp4" />
                </video>
                {ended && (
                  <div className={cn(styles.overlay, styles.overlayText)}>
                    <button className={styles.play} onClick={handlePlay}>
                      <span>Play again</span>
                      <Arrow className={styles.overlayArrow} />
                    </button>
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className={styles.features}>
            {features.map((feature, i) => (
              <div key={i} className={styles.box}>
                {feature.logo}
                <h4 className={styles.heading4}>{feature.title}</h4>
                <p className={cn(styles.featuresSubTitle, styles.paragraph2)}>
                  {feature.paragraph}
                </p>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}
export default Enterprise
