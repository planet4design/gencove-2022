import styles from 'styles/about-agri.module.scss'

function AgriContentThree() {
  return (
    <div className={styles.wrap}>
      <div className="container">
        <div className="row">
          <div className="col-lg-1 col-2">
            <p className={styles.rowNum}>
              1 
            </p>
          </div>
          <div className="col-lg-11 col-10">
            <p className={styles.smallTitle}>
              Automated library preparation
            </p>
            <p className={styles.subTitle}>
              The first step is to prepare your population’s DNA samples at our partner labs. We provide the necessary low-pass sequencing library preparation protocols that are optimized for output and efficiency.
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-1 col-2">
            <p className={styles.rowNum}>
              2
            </p>
          </div>
          <div className="col-lg-11 col-10">
            <p className={styles.smallTitle}>
              Low-pass sequencing
            </p>
            <p className={styles.subTitle}>
              Working with our partners, the samples are sequenced then the data is uploaded to the Gencove platform.
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-1 col-2">
            <p className={styles.rowNum}>
              3
            </p>
          </div>
          <div className="col-lg-11 col-10">
            <p className={styles.smallTitle}>
              Imputation and analysis
            </p>
            <p className={styles.subTitle}>
            The Gencove platform compares the data to a set of fully sequenced genomes. This step, called imputation, results in millions of over 99% accurate variant calls for every sample. Together with our service partners, we can also offer advanced analytics depending on your needs.
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AgriContentThree
