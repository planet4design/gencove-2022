import Lottie from 'lottie-react-web'
import animation from 'assets/vertical-lines.json'

function Shape({ inView }) {
  return (
    <div id="vertical-lines">
      <Lottie
        isPaused={inView ? false : true}
        options={{
          animationData: animation,
          autoplay: false,
          loop: false
        }}
      />

      <style jsx>{`
        div {
          position: absolute;
          width: 389px;
          height: 656px;
          right: 148px;
          bottom: 0;
          z-index: 0;
        }

        @media (max-width: 768px) {
          div {
            right: -180px;
            height: 100%;
            top: 0;
          }
        }
      `}</style>

      <style global jsx>{`
        @media (max-width: 768px) {
          #vertical-lines path {
            stroke-width: 6px !important;
          }
        }
      `}</style>
    </div>
  )
}

export default Shape
