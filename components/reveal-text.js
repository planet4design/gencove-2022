import { useEffect, useRef, useState } from 'react'
import gsap from 'gsap'
import { useInView } from 'react-hook-inview'
import SplitText from 'gsap/dist/SplitText'
import dynamic from 'next/dynamic'
gsap.registerPlugin(SplitText)
// import WebFont from 'webfontloader'

export const preloadFonts = id => {
  const WebFont = require('webfontloader')

  return new Promise(resolve => {
    WebFont.load({
      google: {
        families: id
      },
      active: resolve
    })
  })
}

function Title({
  stagger = 0.15,
  y = 30,
  duration = 0.65,
  delay = 0.15,
  children,
  waitFont = true
}) {
  const [fontReady, setFontReady] = useState(false)

  const [$inView, isVisible] = useInView({
    threshold: 1,
    unobserveOnEnter: true
  })

  const $title = useRef()

  function reveal() {
    const target = $title.current.childNodes[0]
    const results = new SplitText(target, { type: 'word, lines' })

    if (isVisible) {
      gsap.set(target, { visibility: 'visible' })
      gsap.from(results.lines, {
        opacity: 0,
        y,
        stagger,
        duration,
        delay
      })
    }
  }

  useEffect(() => {
    if (waitFont && !isVisible) {
      preloadFonts(['Inter']).then(() => {
        reveal()
      })
    } else {
      reveal()
    }
  }, [isVisible])

  return (
    <div ref={$inView}>
      <div ref={$title} style={{ visibility: 'hidden' }}>
        {children}
      </div>
    </div>
  )
}

export default Title
