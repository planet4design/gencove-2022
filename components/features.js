import styles from 'styles/features.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'

function Features(props) {
  return (
    <div className={styles.wrap}>
      <div className={cn('container', styles.content)}>
      
      <div className="row">
        <div className="col-lg">
          <h2 className={styles.title}>Software-first approach to genomics</h2>
        </div>
      </div>

        <div className="row">
          <div className="col-lg">
            <p className={styles.paragraph}>
              <b>With additional opportunities to improve human health and the challenge of feeding the world’s growing population</b>, demand for genomic information has skyrocketed. Historically, DNA processing fell into two categories: low-throughput and costly options such as whole genome sequencing and high-throughput but limited technologies like genotyping arrays.
            </p>
          </div>


          <div className="col-lg">
            <p className={styles.paragraph}>
              Gencove combines low-pass whole genome sequencing (lower DNA read coverage) with a proprietary software-as-a-service (SaaS) computation layer. The result is a <b>hardware-agnostic, high-volume, and cost-sensitive sequencing products for human, animal, plant, and microbial applications.</b>
            </p>

            <h3>
              <a href="/lp-wgs" className={styles.btnBlue}>What is low-pass sequencing? <span className={styles.outlineArrowBlue}>&gt;</span></a>
            </h3>
          </div>
                          
        </div>

      </div>
    </div>


    
  )
}

export default Features
