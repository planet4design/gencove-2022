import styles from 'styles/about-agri.module.scss'
import cn from 'classnames'

function HumanContentTwo() {
  return (
    <div className={styles.wrapSmaller}>
      <div className={cn('container', styles.content)}>

        <div className="row">
          <div className="col-lg">
           <h2 className={styles.titleSmaller}>The power and discovery capabilities of sequencing</h2>
          </div>
        </div>

        <div className="row">

          <div className="col-lg">
            <img src="/img/human-img-1.jpg" className={styles.img} />
          </div>
          <div className="col-lg">
            <ul className={styles.bulletList}>
              <li className={styles.bulletListItem}>
                Genome-wide association studies
              </li>
              <li className={styles.bulletListItem}>
                New rare genetic variant discovery
              </li>
              <li className={styles.bulletListItem}>
                Population genetic analysis
              </li>
              <li className={styles.bulletListItem}>
                Biobank genomic profiling
              </li>
              <li className={styles.bulletListItem}>
                Pharmacogenomics trials and research
              </li>
              <li className={styles.bulletListItem}>
                Ancestry profiling
              </li>
              <li className={styles.bulletListItem}>
                Diagnostic testing
              </li>
            </ul>

          <h3>
            <a href="http://gencove-6646953.hs-sites.com/the-technical-guide-to-low-pass-sequencing-0/" className={styles.btnBlue} target="_blank">
            Human genomics brochure  <span className={styles.outlineArrowBlue} >&gt;</span></a>
          </h3>
          
          </div>

        </div>

      </div>
    </div>
  )
}

export default HumanContentTwo
