import styles from 'styles/about-company.module.scss'
import cn from 'classnames'

function CompanyContentThree() {
  return (
    <div className={styles.wrapSmaller}>
      <div className={cn('container', styles.content)}>
        
        <div className="row">

          <div className="col-lg">
            <h2 className={styles.titleSmaller}>Are you motivated by contributing to a healthier and more sustainable civilization?</h2>
            <p className={styles.subTitle}>
            We are biologists, geneticists, data scientists, engineers, and business leaders working with our customers, to help develop a software-based genomic technology to advance global health and sustainability.
            </p>
            <h3>
              <a href="https://apply.workable.com/gencove/" className={styles.btnBlue}>View open roles <span className={styles.outlineArrowBlue}>&gt;</span></a>
            </h3>
          </div>
          <div className="col-lg">
            <img src="/img/company-image-2.jpg" />
          </div>

        </div>

      </div>
    </div>
  )
}

export default CompanyContentThree
