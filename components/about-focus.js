import styles from 'styles/about-focus.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'

function Focus() {
  return (
    <div className={styles.wrap}>
      <div className="container">
        <div className={styles.content}>
          <h2 className={styles.title}>Our focus</h2>
          <RevealText stagger={0.075}>
            <h3 className={styles.subTitle}>
              We develop molecular and computational tools to enable high-throughput and low-cost
              genome sequencing solutions for industries from Agriculture to Pharma.
            </h3>
          </RevealText>
        </div>
      </div>
    </div>
  )
}

export default Focus
