import styles from 'styles/human-partners.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'


function Partners(props) {
  return (
    <div className={styles.wrap}>
      <div className={cn('container', styles.content)}>
      
      <div className="row">
        <div className="col-lg">
          <h2 className={styles.smallTitle}>We are proud to be trusted by these leading academic institutions, biotechnology, diagnostic, genomics, and pharma companies</h2>
        </div>
      </div>

        <div className="row">
          <div className="col-lg-2">
            <RevealText>
                <img alt="timer" src="/img/clients-human.png" />
            </RevealText>
          </div>
        </div>
        

          </div>
        </div>

  )
}

export default Partners

