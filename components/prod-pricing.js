import styles from 'styles/about-prod.module.scss'

function Focus() {
  return (
    <div className={styles.wrapWhite}>
      <div className="container">
        <div className="row">
          <div className="col-lg">
            <div className={styles.cardTop}>
              <h2 className={styles.pricingTitle}>Professional</h2>
            </div>
            <div className={styles.card}>
              <h2 className={styles.pricingDetail}>
                <span className={styles.tooltip}>Species-specific* packages
                  <span className={styles.tooltiptext}>
                    Each species package evolves over time as we process more samples, please refer to our <a href="https://docs.gencove.com/main/data-analysis-configurations/" target="_blank">Technical Documentation</a> for the latest information.<br />
                    - <strong>Human</strong> sequencing projects includes copy number variation (CNV) analysis, ancestry analysis, and computation of select polygenic risk scores.<br />
                    - <strong>Cattle</strong> genomics covers breed analysis and additional polymorphisms.<br />
                    - <strong>Canine</strong> package includes breed analysis.<br />
                    - <strong>Maize</strong> sequencing package has strain analysis and multi-allelic SNPs.<br />
                    - <strong>Rat</strong> sequencing includes strain analysis.
                  </span>
                </span>
              </h2>
              <ul className={styles.pricingUL}>
                <li className={styles.pricingULItem}>
                  Gencove platform access 
                </li>
                <li className={styles.pricingULItem}>
                  FastQ file uploads 
                </li>
                <li className={styles.pricingULItem}>
                  Multiple quality checks 
                </li>
              </ul>
              <h2 className={styles.pricingDetail}>Single user</h2>
              <h2 className={styles.pricingDetail}>Data access and management</h2>
              <ul className={styles.pricingUL}>
                <li className={styles.pricingULItem}>
                  API or CLI
                </li>
              </ul>
              <h2 className={styles.pricingDetail}>Low-pass sequencing</h2>
              <ul className={styles.pricingUL}>
                <li className={styles.pricingULItem}>
                  Aligned BAM
                </li>
                <li className={styles.pricingULItem}>
                  Imputed VCF to reference genomes
                </li>
              </ul>
              <h3>
                <a href="/contact-us" className={styles.btnBlue}>
                  Get a quote <span className={styles.outlineArrowBlue}>&gt;</span>
                </a>
              </h3>
            </div>
          </div>
          <div className="col-lg">
            <div className={styles.cardTopTwo}>
              <h2 className={styles.pricingTitle}>Business</h2>
            </div>
            <div className={styles.card}>
              <h2 className={styles.pricingDetail}>
                <span className={styles.tooltip}>Species-specific* packages
                  <span className={styles.tooltiptext}>
                    Each species package evolves over time as we process more samples, please refer to our <a href="https://docs.gencove.com/main/data-analysis-configurations/" target="_blank">Technical Documentation</a> for the latest information.<br />
                    - <strong>Human</strong> sequencing projects includes copy number variation (CNV) analysis, ancestry analysis, and computation of select polygenic risk scores.<br />
                    - <strong>Cattle</strong> genomics covers breed analysis and additional polymorphisms.<br />
                    - <strong>Canine</strong> package includes breed analysis.<br />
                    - <strong>Maize</strong> sequencing package has strain analysis and multi-allelic SNPs.<br />
                    - <strong>Rat</strong> sequencing includes strain analysis.
                  </span>
                </span>
              </h2>
              <ul className={styles.pricingUL}>
                <li className={styles.pricingULItem}>
                  Gencove platform access 
                </li>
                <li className={styles.pricingULItem}>
                  FastQ file uploads 
                </li>
                <li className={styles.pricingULItem}>
                  Multiple quality checks 
                </li>
              </ul>
              <h2 className={styles.pricingDetail}>Unlimited users</h2>
              <h2 className={styles.pricingDetail}>Data access and management</h2>
              <ul className={styles.pricingUL}>
                <li className={styles.pricingULItem}>
                  API or CLI
                </li>
                <li className={styles.pricingULItem}>
                  Special sharing and permissions
                </li>
                <li className={styles.pricingULItem}>
                  Webhooks and custom notifications
                </li>
                <li className={styles.pricingULItem}>
                  Merged VCF
                </li>
              </ul>
              <h2 className={styles.pricingDetail}>Low-pass sequencing</h2>
              <ul className={styles.pricingUL}>
                <li className={styles.pricingULItem}>
                  Aligned BAM
                </li>
                <li className={styles.pricingULItem}>
                  Imputed VCF to reference genomes
                </li>
              </ul>
              <h2 className={styles.pricingDetail}>Advanced analytics</h2>
              <ul className={styles.pricingUL}>
                <li className={styles.pricingULItem}>
                  Custom developed haplotype reference panel for your sole use
                </li>
              </ul>
              <h2 className={styles.pricingDetail}>Client support and services</h2>
              <ul className={styles.pricingUL}>
                <li className={styles.pricingULItem}>
                  Library prep: training manuals and onboarding resources
                </li>
                <li className={styles.pricingULItem}>
                  Dedicated success manager
                </li>
              </ul>
              <h3>
                <a href="/contact-us" className={styles.btnBlue}>
                  Get a quote <span className={styles.outlineArrowBlue}>&gt;</span>
                </a>
              </h3>
            </div>
          </div>
          <div className="col-lg">
            <div className={styles.cardTopThree}>
              <h2 className={styles.pricingTitle}>Enterprise</h2>
            </div>
            <div className={styles.card}>
              <h2 className={styles.pricingDetail}>
                <span className={styles.tooltip}>Species-specific* packages
                  <span className={styles.tooltiptext}>
                    Each species package evolves over time as we process more samples, please refer to our <a href="https://docs.gencove.com/main/data-analysis-configurations/" target="_blank">Technical Documentation</a> for the latest information.<br />
                    - <strong>Human</strong> sequencing projects includes copy number variation (CNV) analysis, ancestry analysis, and computation of select polygenic risk scores.<br />
                    - <strong>Cattle</strong> genomics covers breed analysis and additional polymorphisms.<br />
                    - <strong>Canine</strong> package includes breed analysis.<br />
                    - <strong>Maize</strong> sequencing package has strain analysis and multi-allelic SNPs.<br />
                    - <strong>Rat</strong> sequencing includes strain analysis.
                  </span>
                </span>
              </h2>
              <ul className={styles.pricingUL}>
                <li className={styles.pricingULItem}>
                  Gencove platform access 
                </li>
                <li className={styles.pricingULItem}>
                  FastQ file uploads 
                </li>
                <li className={styles.pricingULItem}>
                  Multiple quality checks 
                </li>
              </ul>
              <h2 className={styles.pricingDetail}>Unlimited users</h2>
              <h2 className={styles.pricingDetail}>Data access and management</h2>
              <ul className={styles.pricingUL}>
                <li className={styles.pricingULItem}>
                  API or CLI
                </li>
                <li className={styles.pricingULItem}>
                  Special sharing and permissions
                </li>
                <li className={styles.pricingULItem}>
                  Webhooks and custom notifications
                </li>
                <li className={styles.pricingULItem}>
                  Merged VCF
                </li>
                <li className={styles.pricingULItem}>
                  HIPAA/Audit trails
                </li>
                <li className={styles.pricingULItem}>
                  Single tenant infrastructure
                </li>
                <li className={styles.pricingULItem}>
                  Extended data lifecycle management
                </li>
                <li className={styles.pricingULItem}>
                  Compliance agreements
                </li>
              </ul>
              <h2 className={styles.pricingDetail}>Low-pass sequencing</h2>
              <ul className={styles.pricingUL}>
                <li className={styles.pricingULItem}>
                  Aligned BAM
                </li>
                <li className={styles.pricingULItem}>
                  Imputed VCF to reference genomes
                </li>
                <li className={styles.pricingULItem}>
                  Targeted capture or Exome upgrade
                </li>
              </ul>
              <h2 className={styles.pricingDetail}>Advanced analytics</h2>
              <ul className={styles.pricingUL}>
                <li className={styles.pricingULItem}>
                  Custom developed haplotype reference panel for your sole use
                </li>
                <li className={styles.pricingULItem}>
                  Proprietary Polygenic Risk Scoring
                </li>
                <li className={styles.pricingULItem}>
                  Associate genetic variation with phenotypes for Genome-Wide Association Studies (GWAS)
                </li>
                <li className={styles.pricingULItem}>
                  <span className={styles.tooltipTwo}>Novel Variant Detection**
                    <span className={styles.tooltiptext}>
                      Low-coverage comparisons within a population to detect novel variants.
                    </span>
                  </span>
                </li>
                <li className={styles.pricingULItem}>
                  <span className={styles.tooltipThree}>Germline variant calling (WGS, WES)***
                    <span className={styles.tooltiptext}>
                      Low-coverage comparisons within a population to detect novel variants.
                    </span>
                  </span>
                </li>
                <li className={styles.pricingULItem}>
                  <span className={styles.tooltipFour}>Somatic variant calling (Tumor/Normal)****
                    <span className={styles.tooltiptext}>
                      <a href="/contact-us">Contact us</a>
                    </span>
                  </span>
                </li>
                <li className={styles.pricingULItem}>
                  <span className={styles.tooltipFive}>Microbiome profiling****
                    <span className={styles.tooltiptext}>
                      <a href="/contact-us">Contact us</a>
                    </span>
                  </span>
                </li>
              </ul>
              <h2 className={styles.pricingDetail}>Client support and services</h2>
              <ul className={styles.pricingUL}>
                <li className={styles.pricingULItem}>
                  Library prep: training manuals and onboarding resources
                </li>
                <li className={styles.pricingULItem}>
                  Dedicated success manager
                </li>
                <li className={styles.pricingULItem}>
                  Custom sequencing protocol development
                </li>
              </ul>
              <h3>
                <a href="/contact-us" className={styles.btnBlue}>
                  Get a quote <span className={styles.outlineArrowBlue}>&gt;</span>
                </a>
              </h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Focus
