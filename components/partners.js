import styles from 'styles/partners.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'


function Partners(props) {
  return (
    <div className={styles.wrap}>
      <div className={cn('container', styles.content)}>
      
        <div className="row">
          <div className="col-lg">
            <RevealText>
              <h2 className={styles.title}>
                Top academic institutions plus agriculture, biotech, diagnostics, and pharmaceutical companies rely on our low-pass sequencing and analysis products every day
              </h2>
            </RevealText>
          </div>
        </div>

        <div className="row">
          <div className="col-lg">
            <RevealText>
            <p className={styles.paragraph}>
                <img alt="timer" src="/img/clients.png" />
            </p>
            </RevealText>
          </div>
        </div>

        <div className="row">
          <div className="col-lg">
            <RevealText>
              <h2 className={styles.title}>
                To offer the most advanced sequencing solutions, the largest genomics service providers in the world partner with Gencove
              </h2>
            </RevealText>
          </div>
        </div>

        <div className="row">
          <div className="col-lg">
            <RevealText>
              <p className={styles.paragraph}>
                <img alt="timer" src="/img/clients4.png" />
              </p>
            </RevealText>
          </div>
        </div>

      </div>
    </div>
  )
}

export default Partners

