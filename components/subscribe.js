import { useState } from 'react'
import axios from 'axios'
import cn from 'classnames'
import styles from 'styles/footer.module.scss'
import { isEmail } from 'validator'
import { useRouter } from 'next/router'

const portalId = '6646953'
const formGuid = 'f6a39209-8e87-4447-9566-ae1c8e98697f'

function Subscribe(props) {
  const router = useRouter()
  const [email, setEmail] = useState('')
  const [error, setError] = useState('')
  const [success, setSuccess] = useState(false)
  const [loading, setLoading] = useState(false)

  function handleSubmit(event) {
    event.preventDefault()

    if (!email) {
      setError('Email field is required.')
      return
    }

    if (!isEmail(email)) {
      setError('Invalid email format.')
      return
    }

    setError('')
    setSuccess(false)
    setLoading(true)

    axios({
      method: 'POST',
      url: `https://api.hsforms.com/submissions/v3/integration/submit/${portalId}/${formGuid}`,
      data: {
        fields: [{ name: 'email', value: email }],
        context: {
          pageName: 'Subscription Footer',
          pageUri: router.pathname
        }
      }
    })
      .then(response => {
        if (response?.status === 'error') {
          setError(response?.message)
        } else {
          setSuccess(true)
          setEmail('')
        }

        setLoading(false)
      })
      .catch(error => {
        const response = error?.response?.data
        setError(response?.message + '.')
        setLoading(false)
      })
  }

  function handleChange(event) {
    setEmail(event.target.value)
  }

  if (success) {
    return <div className={styles.success}>Thanks for subscribing to Gencove's newsletter. </div>
  }

  return (
    <form
      className={cn(styles.field, loading && styles.loading)}
      onSubmit={handleSubmit}
      noValidate
    >
      <label htmlFor="subscribe-email" className="sr-only">
        Email:
      </label>
      <input
        id="subscribe-email"
        type="email"
        name="email"
        className={styles.input}
        placeholder="Subscribe to our newsletter"
        value={email}
        onChange={handleChange}
        disabled={loading}
      />
      <button className={styles.btn_submit} title="Submit" disabled={loading}>
        <span className="sr-only">Submit</span>
      </button>
      {error && <div className={styles.error}>{error} Please review email address.</div>}
    </form>
  )
}


            <ul className="pl-0 mt-6 mb-4 mt-lg-12 pt-lg-6">
              <li className={styles.copy}>© Copyright 2022 Gencove</li>
              <li className={styles.copy}>
                <a className="ml-4" href="https://web.gencove.com/privacy" target="_blank">
                  Privacy Policy
                </a>
              </li>
            </ul>
export default Subscribe
