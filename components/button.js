import Link from 'next/link'
import cn from 'classnames'

function Button({ children, href, variant = 'primary', type = 'button', className, ...rest }) {
  const classNames = cn(
    'btn',
    {
      'btn-primary': variant === 'primary',
      'btn-link-primary': variant === 'link',
      'btn-outline-light': variant === 'outline'
    },
    className
  )

  const sharedProps = {
    className: classNames,
    ...rest
  }

  if (href) {
    if (href.includes('http')) {
      return (
        <a href={href} {...sharedProps}>
          {children}
        </a>
      )
    } else {
      return (
        <Link href={href}>
          <a {...sharedProps}>{children}</a>
        </Link>
      )
    }
  }

  return (
    <button type={type} {...sharedProps}>
      {children}
    </button>
  )
}

export default Button
