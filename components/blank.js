import styles from 'styles/press-hero.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'
import { useInView } from 'react-hook-inview'

function Blank() {
  const [ref, inView] = useInView({ threshold: 0, unobserveOnEnter: true })

  return (
    <div className={cn(styles.wrap, 'hero')}>
      <div className={cn('container', styles.content)}>
        <div className={styles.overlay}></div>
        <div className="row">
          <div className="col-lg">
            <RevealText stagger={0.125}>
              <h1 className={styles.title}>
                What is low-pass whole genome sequencing?
              </h1>
            </RevealText>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Blank
