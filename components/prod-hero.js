import Link from 'next/link'
import styles from 'styles/about-hero.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'
import { useInView } from 'react-hook-inview'
import Arrow from 'components/icons/arrow'

function ProdHero() {
  const [ref, inView] = useInView({ threshold: 0, unobserveOnEnter: true })

  return (
    <div className={cn(styles.wrap, 'hero')}>
      <div className={cn('container', styles.content)}>
        <video className={styles.video} muted playsInline loop autoPlay poster="">
          <source src="video/Product.webm" type="video/webm" />
          <source src="video/Product.mp4" type="video/mp4" />
        </video>
        <div className={styles.overlay}></div>
        <div className="row">
          <div className="col-lg">
            <RevealText stagger={0.125}>
              <h1 className={styles.title}>
                High-volume and cost-effective sequencing software 
              </h1>
            </RevealText>
            <Link href="/contact-us">
              <a href="/contact-us" className={styles.link}>
                <p>Talk to our experts <span className={styles.outlineArrow}>&gt;</span></p>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProdHero



