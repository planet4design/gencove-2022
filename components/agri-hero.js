import Link from 'next/link'
import styles from 'styles/about-hero.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'
import { useInView } from 'react-hook-inview'

function Hero() {
  const [ref, inView] = useInView({ threshold: 0, unobserveOnEnter: true })

  return (
    <div className={cn(styles.wrap, 'hero')}>
      <div className={cn('container', styles.content)}>
        <video className={styles.video} muted playsInline loop autoPlay poster="img/bg-hero-home.jpg">
          <source src="video/agrigenomics.webm" type="video/webm" />
          <source src="video/agrigenomics.mp4" type="video/mp4" />
        </video>
        
        <div className={styles.overlay}></div>
        <div className="row">
          <div className="col-lg">
            <RevealText stagger={0.125}>
              <h1 className={styles.title}>
                Sequencing solutions for global health and sustainability
              </h1>
            </RevealText>
            <Link href="/contact-us">
            <a href="/contact-us" className={styles.link}>
                <p>Talk to our experts <span className={styles.outlineArrow}>&gt;</span></p>
            </a>
          </Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Hero
