import styles from 'styles/about-careers.module.scss'
import RevealText from 'components/reveal-text'
import Button from 'components/button'

function Careers() {
  return (
    <div className={styles.wrap}>
      <div className="container">
        <RevealText>
          <h2 className={styles.title}>
            Are you looking to work on exciting challenges in Genomics?
          </h2>
        </RevealText>
        <p className={styles.subTitle}>
          We are a group of scientists, engineers, and entrepreneurs who are building a future where
          genome sequencing is accessible and interpretable.
        </p>
        <Button
          href={'https://apply.workable.com/gencove/'}
          variant={'primary'}
          className={styles.btnBlue}
        >
          View open positions
        </Button>
      </div>
    </div>
  )
}

export default Careers
