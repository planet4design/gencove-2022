import { useState } from 'react'
import { useForm } from 'react-hook-form'
import styles from 'styles/form.module.scss'
import cn from 'classnames'
import Button from 'components/button'
import axios from 'axios'
import { isEmail } from 'validator'
import { useRouter } from 'next/router'

const portalId = '6646953'
const formGuid = '9ae81fd3-d69a-47a2-b53b-db67961320e9'

export default function Form() {
  const { pathname } = useRouter()
  const { register, handleSubmit, errors } = useForm()
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState('')
  const [success, setSuccess] = useState(false)

  const onSubmit = (data, e) => {
    e.preventDefault()

    setError('')
    setSuccess(false)
    setLoading(true)

    axios({
      method: 'POST',
      url: `https://api.hsforms.com/submissions/v3/integration/submit/${portalId}/${formGuid}`,
      data: {
        fields: [
          { name: 'firstname', value: data.firstname },
          { name: 'company', value: data.company },
          { name: 'email', value: data.email },
          { name: 'application_of_interest', value: data.application_of_interest },
          { name: 'message', value: data.message }
        ],
        context: {
          pageName: 'Contact Page',
          pageUri: pathname
        }
      }
    })
      .then(response => {
        if (response?.status === 'error') {
          setError(response?.message)
        } else {
          setSuccess(true)
          e.target.reset()
        }

        setLoading(false)
      })
      .catch(error => {
        const response = error?.response?.data
        setError(response?.message + '.')
        setLoading(false)
      })
  }

  if (success) {
    return (
      <div className="success">
        <p>Thanks for getting in contact with us.</p>
        <p>We will reach out right away.</p>

        <style jsx>{`
          .success {
            font-size: 20px;
            line-height: 1.6;
            letter-spacing: -0.03em;
          }
        `}</style>
      </div>
    )
  }

  return (
    <form className={styles.form} onSubmit={handleSubmit(onSubmit)} noValidate>
      <legend className="sr-only">
        <h2>Contact form</h2>
      </legend>

      <div className={styles.group}>
        <div className={styles.inputField}>
          <input
            id="firstname"
            type="text"
            className={cn(styles.input, errors.name && styles.inputError)}
            name="firstname"
            placeholder=" "
            ref={register({
              required: 'Name is required',
              maxLength: {
                value: 200,
                message: 'The name must have at the most 200 characters'
              }
            })}
          />
          <label
            htmlFor="firstname"
            className={cn(styles.label, errors.firstname && styles.labelError)}
          >
            Name
          </label>
        </div>
        {errors.firstname && <span className={styles.error}>{errors.firstname.message}</span>}
      </div>

      <div className={styles.group}>
        <div className={styles.inputField}>
          <input
            id="company"
            type="text"
            className={cn(styles.input, errors.company && styles.inputError)}
            name="company"
            placeholder=" "
            ref={register({
              required: 'Organization is required',
              maxLength: {
                value: 200,
                message: 'The name must have at the most 200 characters'
              }
            })}
          />

          <label
            htmlFor="company"
            className={cn(styles.label, errors.organization && styles.labelError)}
          >
            Organization
          </label>
        </div>

        {errors.company && <span className={styles.error}>{errors.company.message}</span>}
      </div>

      <div className={styles.group}>
        <div className={styles.inputField}>
          <input
            id="email"
            type="email"
            className={cn(styles.input, errors.email && styles.inputError)}
            name="email"
            placeholder=" "
            ref={register({
              required: 'Email is required',
              validate: value => (isEmail(value) ? true : 'Invalid email address.')
            })}
          />
          <label htmlFor="email" className={cn(styles.label, errors.email && styles.labelError)}>
            Email address
          </label>
        </div>

        {errors.email && <span className={styles.error}>{errors.email.message}</span>}
      </div>

      <div className={styles.group}>
        <div className={styles.inputField}>
          <div className={styles.selectWrap}>
            <select
              id="application_of_interest"
              className={cn(styles.select)}
              name="application_of_interest"
              ref={register({
                required: 'Application of interest is required'
              })}
              defaultValue=""
              required
            >
              <option value="" disabled></option>
              <option value="Human genomics">Human Genomics</option>
              <option value="Agriculture">Agricultural Genomics</option>
              <option value="Companion Animal">Companion Animals Genomics</option>
            </select>

            <label className={cn(styles.label)} htmlFor="application_of_interest">
              Application of interest
            </label>
          </div>
        </div>

        {errors.application_of_interest && (
          <span className={styles.error}>{errors.application_of_interest.message}</span>
        )}
      </div>

      <div className={styles.group}>
        <div className={styles.inputField}>
          <input
            id="message"
            className={styles.input}
            placeholder=" "
            name="message"
            type="text"
            ref={register}
          />
          <label htmlFor="message" className={styles.label}>
            Inquiry (optional)
          </label>
        </div>
      </div>

      {error && (
        <div className="error">
          {error}

          <style jsx>{`
            .error {
              color: #e0182d;
            }
          `}</style>
        </div>
      )}

      <Button variant={'primary'} className={styles.btnPrimary} type="submit" disabled={loading}>
        {loading ? 'Loading…' : 'Contact Us'}
      </Button>
    </form>
  )
}
