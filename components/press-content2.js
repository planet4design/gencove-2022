import styles from 'styles/press.module.scss'
import cn from 'classnames'

function CompanyContentTwo() {
  return (
    <div className={styles.wrapSmaller}>
      <div className={cn('container', styles.content)}>
        
        <div className="row">

          <div className="col-lg">

            <p className={styles.subTitle}>October 26, 2021  -  Gencove</p>

            <h2 className={styles.titleSmaller}>Gencove Raises $10 Million to Meet the Global Demand for Accessible and Interpretable Whole Genome Sequencing at Scale</h2>
            <p className={styles.subTitle}>
The company&rsquo;s low-pass genome sequencing and bioinformatics platform provides a new way to power complete, accurate, and affordable genomic discovery</p>

            <p className={styles.subTitle}>
<strong>NEW YORK.</strong> – Oct. 26, 2021 /PRNewswire/ -- Gencove, the leading low-pass genome sequencing platform company, today announced the successful close of a $10 million Series A investment bringing the company&rsquo;s total financing to $16.2 million. The round was led by Lewis &amp; Clark AgriFood with participation from new and existing investors including Spero Ventures, Techammer, Third Kind Venture Capital, and Version One Ventures. Funding will be used for further product development, commercial growth and to build out key business functions.</p>

<p className={styles.subTitle}>Dr. Joseph Pickrell, CEO and Dr. Tomaz Berisa, CTO the founders of Gencove together announce the successful close of Series A funding.</p>

<p className={styles.subTitle}>DNA sequencing has become the foundation for biological advancements by enabling the exploration of genetic variants and structural changes in DNA. Historically, technologies for genetic testing fall into two categories: low-throughput and costly options such as whole genome sequencing, and high-throughput but limited technologies like genotyping arrays.</p>

<p className={styles.subTitle}>Gencove solves the too much or too little quandary by combining low-pass whole genome sequencing (reading the DNA fewer times) with a proprietary software-as-a-service (SAAS) computation layer. The result is a unique high-throughput and cost-effective whole genome sequencing solution.</p>

<p className={styles.subTitle}>&quot;We believe the next revolution in genomics will be driven by software,&quot; said Joseph Pickrell, Ph.D., co-founder, and CEO of Gencove. &quot;It&rsquo;s rewarding to work with investors who recognize the value our platform provides to the agricultural, biotechnology, clinical testing, and pharmaceutical industries.&quot;</p>

<p className={styles.subTitle}>In addition to Gencove&rsquo;s low-pass whole genome sequencing and SAAS platform, the company has developed large cross-species genomic datasets and reference panels.  For its customers, these data equate to greater accuracy, more meaning, and increased research flexibility, today and in the future.</p>

<p className={styles.subTitle}>&quot;Gencove&rsquo;s platform and datasets can help unlock the genetic underpinnings of our health and wellbeing,&quot; said Larry Page, Ph.D., managing director at Lewis and Clark AgriFood.  &quot;We are thrilled to be part of a company whose technology has such broad consumer and industrial applications from agrigenomics, biotechnology, virology to medical diagnoses, disease susceptibility, and response to medical treatment.&quot;</p>

<p className={styles.subTitle}><strong>&quot;The new round of funding will enable us to systematically address the significant demand for industrial-scale genomics we&rsquo;re seeing in the market,&quot; said Tomaz Berisa, Ph.D., Gencove&rsquo;s co-founder, and CTO. &quot;We look forward to bringing our novel genomics platform to more customers as quickly as possible to support their important work.&quot;</strong></p>

<p className={styles.subTitle}><strong>About Gencove</strong></p>

<p className={styles.subTitle}>With additional threats to human health and the challenge of feeding the world&rsquo;s growing population, demand for genomic information has skyrocketed. Historically, DNA processing options fell into two categories: low-throughput and costly options such as whole genome sequencing, and high-throughput but limited technologies like genotyping arrays. To help scientists make life-saving discoveries, Gencove combines low-pass whole genome sequencing (reading the DNA at shallow depths) with a proprietary software-as-a-service (SaaS) computation layer. The result is a fast, high-volume, and cost-sensitive sequencing solution. As evidence of the technology&rsquo;s value, the company has hundreds of customers, and the largest genomics service providers in the world partner with Gencove.&nbsp; For more information visit: www.gencove.com and follow us on www.linkedin.com/company/gencove/mycompany/ and twitter.com/Gencove.</p>

<p className={styles.subTitle}><strong>About BGI Americas Corporation</strong></p>

<p className={styles.subTitle}>BGI Americas Corporation is one of the leading providers of genomics and proteomics services in the Americas Region as part of BGI Genomics, a public company listed on the Shenzhen Stock Exchange. Established in 2010, BGI Americas has grown to include a presence in Boston and San Jose, serving a wide range of customers in genetics research, drug R&amp;D, and diagnostics. BGI brings over 20 years of genomics experience to its customers and collaborators based on a broad array of leading technologies, including proprietary DNBSEQ genetic sequencing platforms, economies of scale, and expert bioinformatics resources. BGI is committed to advancing genetic research, technologies, and applications to benefit humankind.</p>

<p className={styles.subTitle}>Contacts</p>
<p className={styles.subTitle}>For Gencove:</p>
<p className={styles.subTitle}>Kristi Ashton, 650.224.8231</p>
<p className={styles.subTitle}>kristi.ashton@gencove.com</p>
<p className={styles.subTitle}>For Lewis &amp; Clark AgriFood:</p>
<p className={styles.subTitle}>Jessie Chapel, 314-651-4915</p>
<p className={styles.subTitle}>jessie@lacpartners.com</p>




      </div>
    </div>
      </div>
    </div>
  )
}

export default CompanyContentTwo

