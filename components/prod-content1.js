import styles from 'styles/about-prod.module.scss'

function Careers() {
  return (
    <div className={styles.wrapBlack}>
      <div className="container">
        <p className={styles.title}>
          Our vision is to make genomic information ubiquitous through an innovative, software-first approach
        </p>
      </div>
    </div>
  )
}

export default Careers
