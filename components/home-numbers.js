import styles from 'styles/features.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'

function Numbers(props) {
  return (
    <div className={styles.wrap}>
      <div className={cn('container', styles.content)}>
      
      <div className="row">
        <div className="col-lg">
          <h2 className={styles.title}>By putting more genomic information in the hands of scientists, the world’s most pressing biological challenges can be solved</h2>
        </div>
      </div>


        <div className="row">
          <div className="col-lg">
            <RevealText>
            <p className={styles.paragraph}>
                <img alt="timer" src="/img/10x-1.png" />
            </p>
            </RevealText>
          </div>


          <div className="col-lg">
            <RevealText>
            <p className={styles.paragraph}>
                <img alt="timer" src="/img/10x-2.png"/>
            </p>
            </RevealText>
          </div>
        </div>


        <div className="row">
          <div className="col-lg">
            <RevealText>
            <p className={styles.paragraph}>
                <img alt="timer" src="/img/10x-3.png" />
            </p>
            </RevealText>
          </div>

          <div className="col-lg">
            <RevealText>
            <p className={styles.paragraph}>
                <img alt="timer" src="/img/99percent.png" />
            </p>
            </RevealText>
          </div>


        </div>

        <div className="row">
          <div className="col-lg">
            <RevealText>
              <h3>
                <a href="/products" className={styles.btnBlue}>See all our products have to offer <span className={styles.outlineArrowBlue} >&gt;</span></a>
              </h3>
            </RevealText>
          </div>
        </div>

      </div>
    </div>
  )
}

export default Numbers

