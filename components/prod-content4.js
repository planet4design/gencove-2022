import styles from 'styles/about-prod.module.scss'
import CloudScale from 'components/icons/cloud-scale'
import Security from 'components/icons/security'
import Compliance from 'components/icons/compliance'
import Speed from 'components/icons/speed'
import Collaboration from 'components/icons/collaboration'
import Integration from 'components/icons/integration'
import Customization from 'components/icons/customization'
import Support from 'components/icons/support'
import RevealText from 'components/reveal-text'

function ProuctContentThree() {
  return (
    <div className={styles.wrapWhite}>
      <div className="container">
        <div className="row">
          <div className="col-lg">
            <RevealText>
              <h2 className={styles.title}>
                Gencove’s platform is scalable and fast to set up for your organization’s high-throughput genotyping and analysis programs
              </h2>
            </RevealText>
            <div className="row">
              <div className="col-lg-3 col-6">
                <CloudScale />
                <h3 className={styles.smallTitleBlack}>
                  Cloud-scale
                </h3>
                <p className={styles.subTitle}>
                  Gencove is cloud-native, meaning end-to-end analysis is done in the cloud
                </p>
              </div>
              <div className="col-lg-3 col-6">
                <Security />
                <h3 className={styles.smallTitleBlack}>
                  Security
                </h3>
                <p className={styles.subTitle}>
                  Rest assured Gencove is using industry-accepted best practices and frameworks
                </p>
              </div>
              <div className="col-lg-3 col-6">
                <Compliance />
                <h3 className={styles.smallTitleBlack}>
                  Compliance
                </h3>
                <p className={styles.subTitle}>
                  Gencove is HIPAA compliant and aligns itself with ISO27001 and SOC2
                </p>
              </div>
              <div className="col-lg-3 col-6">
                <Speed />
                <h3 className={styles.smallTitleBlack}>
                  Speed
                </h3>
                <p className={styles.subTitle}>
                  Fast analytics together with lightning-fast uploads and downloads through CLI tools
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3 col-6">
                <Collaboration />
                <h3 className={styles.smallTitleBlack}>
                  Collaboration
                </h3>
                <p className={styles.subTitle}>
                  Share projects with colleagues across your organization
                </p>
              </div>
              <div className="col-lg-3 col-6">
                <Integration />
                <h3 className={styles.smallTitleBlack}>
                  Integration
                </h3>
                <p className={styles.subTitle}>
                  Integrate directly with our API for production-ready workloads
                </p>
              </div>
              <div className="col-lg-3 col-6">
                <Customization />
                <h3 className={styles.smallTitleBlack}>
                  Customization
                </h3>
                <p className={styles.subTitle}>
                  Private configurations and reference panels for custom populations can be made available
                </p>
              </div>
              <div className="col-lg-3 col-6">
                <Support />
                <h3 className={styles.smallTitleBlack}>
                  Support
                </h3>
                <p className={styles.subTitle}>
                  If you run into a snag, our team is there to help quickly and efficiently
                </p>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  )
}

export default ProuctContentThree
