import styles from 'styles/news.module.scss'
import Link from 'next/link'
import IconArrow from 'components/icons/arrow'
import news from 'data/news.json'
import resources from 'data/resources.json'
import Chevron from 'components/icons/chevron'
import Slider from 'react-slick'
import Card from 'components/news-card'
import RevealText from 'components/reveal-text'
import cn from 'classnames'
import { useState } from 'react'

function News() {
  const settings = {
    dots: false,
    accessibility: true,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: <Chevron />,
    prevArrow: <Chevron />,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2
        }
      }
    ]
  }
  return (
    <div id="news" className={styles.wrap}>
      <div className="container">
        <h2 className={styles.title}>Resources</h2>
        <div className={cn(styles.resources)}>
          <div className={styles.content}>
            <Slider className={styles.slider} {...settings}>
              {resources.map((article, i) => (
                <Card key={i} article={article} />
              ))}
            </Slider>
          </div>
        </div>
      </div>
    </div>
  )
}

export default News
