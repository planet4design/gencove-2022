import styles from 'styles/about-company.module.scss'
import RevealText from 'components/reveal-text'

function QuoteThree() {
  return (
    <div className={styles.wrapQuote}>
      <div className="container">

        <div className="row">
          <div className="col-lg-1">
            <RevealText>
              <img src="/img/testimonials/quotes.png" alt="quotes" />
            </RevealText>
          </div>
          <div className="col-lg-11">
            <RevealText>
              <h4 className={styles.quote}>
                Gencove’s platform can help unlock the genetic underpinnings of our health and wellbeing.
              </h4>
            </RevealText>
            <p className={styles.quoter}>
              <span className={styles.bold}>Larry Page, Ph.D.</span><br />
              Managing Director at Lewis and Clark AgriFood
            </p>
          </div>
        </div>

      </div>
    </div>
  )
}

export default QuoteThree
