import styles from 'styles/human-partners.module.scss'
import cn from 'classnames'
import RevealText from 'components/reveal-text'


function Partners(props) {
  return (
    <div className={styles.wrap}>
      <div className={cn('container', styles.content)}>
      
      <div className="row">
        <div className="col-lg">
          <h2 className={styles.smallTitle}>Gencove is backed by world-class investors</h2>
        </div>
      </div>


        <div className="row">
          <div className="col-lg-12">
            <RevealText>
            <p className={styles.paragraph}>
                <img alt="company" src="/img/comany-image-2.png" />
            </p>
            </RevealText>
          </div>
        </div>

      </div>
    </div>
  )
}

export default Partners

