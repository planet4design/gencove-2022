import Lottie from 'lottie-react-web'
import animation from 'assets/horizontal-lines.json'

function Shape({ inView }) {
  return (
    <div id="horizontal-lines">
      <Lottie
        isPaused={inView ? false : true}
        options={{
          animationData: animation,
          autoplay: false,
          loop: false
        }}
      />

      <style jsx>{`
        div {
          position: absolute;
          width: 369px;
          height: 323px;
          top: 120px;
          right: 0px;
        }

        @media (max-width: 768px) {
          div {
            top: 25px;
            width: 220px;
            height: 195px;
          }
        }

        @media (max-width: 480px) {
          div {
            top: 25px;
            width: 174px;
            height: 153px;
          }
        }
      `}</style>

      <style global jsx>{`
        @media (max-width: 768px) {
          #horizontal-lines path {
            stroke-width: 6px !important;
          }
        }
      `}</style>
    </div>
  )
}

export default Shape
