import styles from 'styles/black-section.module.scss'

function Careers() {
  return (
    <div className={styles.wrap}>
      <div className="container">
        <div className="row">
          <div className="col-lg">
            <p className={styles.subTitle}>
              The goal of human genomics is to
identify genetic variants in the population that influence phenotypes, such as disease susceptibility or response to medical treatment. Historically, this has posed a challenge. The only cost-effective technology for profiling large numbers of individuals has been genotyping arrays, which only measure a small fraction of the genome.
            </p>
          </div>
          <div className="col-lg">
          <p className={styles.subTitle}>
            Sequencing-based technologies, which
enable more comprehensive profiling of genetic variation, remain too expensive for routine use. Gencove&apos;s low-pass technology and analytics software provide a path to ubiquitous genomic information.
          </p>
          <h3>
            <a href="http://gencove-6646953.hs-sites.com/the-technical-guide-to-low-pass-sequencing" className={styles.btnBlue} target="_blank">
            Get a technical guide  <span className={styles.outlineArrowBlue} >&gt;</span></a>
            
          </h3>
        </div>
        </div>
      </div>
    </div>
  )
}

export default Careers
