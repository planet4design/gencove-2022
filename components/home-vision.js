import styles from 'styles/black-section.module.scss'

function Careers() {
  return (
    <div className={styles.wrap}>
      <div className="container">

      <div className="row">
        <div className="col-lg">
          <h2 className={styles.title2}>Our vision is to make genomic information ubiquitous</h2>
        </div>
      </div>
        <div className="row">
          <div className="col-lg-4">
            <p className={styles.paragraph}>
                <img alt="timer" src="/img/25.png" />
            </p>
          </div>
          <div className="col-lg">
            <p className={styles.paragraph}>
                <img alt="timer" src="/img/250.png" />
            </p>
        </div>
        </div>
      </div>
    </div>
  )
}

export default Careers

