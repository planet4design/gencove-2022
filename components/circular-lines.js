import Lottie from 'lottie-react-web'
import animation from 'assets/circular-lines.json'

function Shape({ inView }) {
  return (
    <div id="circular-lines">
      <Lottie
        isPaused={inView ? false : true}
        options={{
          animationData: animation,
          autoplay: false,
          loop: false
        }}
      />

      <style jsx>{`
        div {
          position: absolute;
          width: 728px;
          height: 728px;
          top: -110px;
          right: -92px;
        }

        @media (max-width: 768px) {
          div {
            width: 480px;
            height: 480px;
          }
        }

        @media (max-width: 480px) {
          div {
            width: 268px;
            height: 268px;
            top: -33px;
            right: -54px;
          }
        }
      `}</style>

      <style global jsx>{`
        @media (max-width: 768px) {
          #circular-lines path {
            stroke-width: 4px !important;
          }
        }
      `}</style>
    </div>
  )
}

export default Shape
