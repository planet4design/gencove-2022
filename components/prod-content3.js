import styles from 'styles/about-prod.module.scss'
import RevealText from 'components/reveal-text'
import Shape from './circular-lines-black'

function ProuctContentThree() {
  return (
    <div className={styles.wrapSectionThree}>
      <div className="container">
        <div className="row">
          <div className="col-lg-8">
            <RevealText>
              <h2 className={styles.title}>
                More power and flexibility
              </h2>
            </RevealText>
            <h3 className={styles.smallTitleBlack}>
              Start with ready-to-run or custom pipelines
            </h3>
            <p className={styles.subTitle}>
            Choose an existing pipeline or work with Gencove&rsquo;s scientists to optimize the platform for your application
            </p>
            <h3 className={styles.smallTitleBlack}>
              Select the appropriate deployment model that fits your needs
            </h3>
            <div className="row">
              <div className="col-lg-4">
                <h3 className={styles.subTitleCTA}>
                  End-to-end
                </h3>
                <p className={styles.subTitle}>
                  From biological sample to data through our sequencing partners that have already integrated with Gencove
                </p>
              </div>
              <div className="col-lg-4">
                <h3 className={styles.subTitleCTA}>
                  In-house
                </h3>
                <p className={styles.subTitle}>
                  Set up the low-pass lab workflow in your lab and seamlessly use the Gencove imputation and analysis SaaS
                </p>
              </div>
              <div className="col-lg-4">
                <h3 className={styles.subTitleCTA}>
                  SaaS-only
                </h3>
                <p className={styles.subTitle}>
                  Integrate Gencove directly with your existing systems or greenfield deployments
                </p>
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            <Shape />
          </div>
        </div>
        
      </div>
    </div>
  )
}

export default ProuctContentThree
