import styles from 'styles/why-gencove.module.scss'
import RevealText from 'components/reveal-text'

function WhyGencove() {
  return (
    <div className={styles.wrap}>
      <div className="container">
        <h2 className={styles.title}>Why Gencove?</h2>
        <RevealText>
          <h3 className={styles.subTitle}>Software-first approach to industrial genomics</h3>
        </RevealText>
        <div className={styles.partner}>
          <img className={styles.logo} src="/img/why-gencove/neogen-logo.png" alt="NEOGEN" />
          <p className={styles.logoText}>
            Gencove helped Neogen across its organization as follows:
          </p>
        </div>
        <div className={styles.numbers}>
          <div className={styles.box}>
            <div className={styles.number}>2</div>
            <p className={styles.numberText}>weeks to set up a custom poultry imputation panel</p>
          </div>
          <div className={styles.box}>
            <div className={styles.number}>2K</div>
            <p className={styles.numberText}>samples processed in a day</p>
          </div>
          <div className={styles.box}>
            <div className={styles.number}>20M</div>
            <p className={styles.numberText}>genetic variants called at 99% accuracy</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default WhyGencove
