import styles from 'styles/footer.module.scss'
import Link from 'next/link'
import Logo from 'components/logo'
import Subscribe from 'components/subscribe'
import IconLinkedIn from 'components/icons/linkedin'
import IconMedium from 'components/icons/medium'
import IconTwitter from 'components/icons/twitter'

function Footer() {
  return (
    <div className={styles.wrap}>
      <div className="container-fluid pl-md-5 pr-md-5 pt-5">
        <div className="row">
          <div className="col-12 col-lg-5">
            <h2>
              <Link href="/">
                <a>
                  <Logo aria-label="Gencove" className={styles.logo} width="154" height="44" />
                </a>
              </Link>
            </h2>
          </div>
      
      
      
          <div className="col-12 col-lg-7 pt-md-2">
            <ul className={styles.nav}>
                
                
              <li className={styles.nav_item}>
                <Link href="/company">
                  <a className={styles.nav_link}>Company</a>
                </Link>
                  
                  
                <Link href="https://apply.workable.com/gencove/" passHref={true}>
                  <a className={styles.nav_link}>Careers</a>
                </Link>
                  
                  
                <Link href="/contact-us" passHref={true}>
                  <a className={styles.nav_link}>Contact us</a>
                </Link>
               </li>
                  
                
                <li className={styles.nav_item}>
                <Link href="https://docs.gencove.com/main/introduction/">
                  <a className={styles.nav_link}>API docs</a>
                </Link>
                    
                <Link href="https://web.gencove.com/login" passHref={true}>
                  <a className={styles.nav_link}>Sign in</a>
                </Link>
              </li>

                <li className={styles.nav_item}>
                <ul className={styles.social_media}>
                  <li className="d-inline-block">
                    <Link href="https://www.linkedin.com/company/gencove/" passHref={true}>
                      <a className={styles.social_link} target="_blank" rel="noopener noreferrer">
                        <IconLinkedIn className={styles.icon} width="30" height="30" />
                        <span className="sr-only">LinkedIn</span>
                      </a>
                    </Link>
                  </li>
                  <li className="d-inline-block ml-3">
                    <Link href="https://twitter.com/gencove" passHref={true}>
                      <a className={styles.social_link} target="_blank" rel="noopener noreferrer">
                        <IconTwitter className={styles.icon} width="36" height="30" />
                        <span className="sr-only">Twitter</span>
                      </a>
                    </Link>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
      
      
          <div className="col-12 col-lg-5">
            <Subscribe />
          </div>
          <div className="col-12">
            <ul className="pt-lg-6">
              <li className={styles.copy}>© Copyright 2022 Gencove</li>
              <li className={styles.copy}>
                <a className="ml-4" href="https://web.gencove.com/privacy" target="_blank">
                  Privacy Policy
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Footer
