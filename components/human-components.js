import { useRef, useEffect, useState } from 'react'
import gsap from 'gsap'
import debounce from 'lodash.debounce'
import { useInView } from 'react-hook-inview'
import styles from 'styles/agri-components.module.scss'
import cn from 'classnames'

const fadeIn = { autoAlpha: 0, duration: 0.5 }
const lineIn = { autoAlpha: 0, scaleY: 0, duration: 0.35 }
const lineOut = { autoAlpha: 0, scaleY: 0, duration: 0.15, immediateRender: true }
const hideNow = { autoAlpha: 0, immediateRender: true, duration: 0.25 }
const copyOut = { autoAlpha: 0, duration: 0.15, immediateRender: true }
// const hideLeave = { autoAlpha: 0, immediateRender: true, duration: 0.15 }

function Diagram(props) {
  const [completed, setCompleted] = useState(false)

  const [$ref, isVisible] = useInView({
    threshold: 1,
    unobserveOnEnter: true
  })

  const $iconSamples = useRef()
  // const $arrowLeft = useRef()
  const $circle1 = useRef()
  const $circle2 = useRef()
  const $circle3 = useRef()
  const $copy1 = useRef()
  const $copy2 = useRef()
  const $copy3 = useRef()
  const $line1 = useRef()
  const $line2 = useRef()
  const $line3 = useRef()
  const $label1 = useRef()
  const $label2 = useRef()
  const $label3 = useRef()
  const $arrowRight = useRef()
  const $iconAnalytics = useRef()

  const groups = [
    { line: $line1, copy: $copy1 },
    { line: $line2, copy: $copy2 },
    { line: $line3, copy: $copy3 }
  ]

  let tl = gsap.timeline({ overwrite: true, onComplete: () => setCompleted(true) })
  tl.timeScale(1.7) // velocidad total de la animacion de entrada

  useEffect(() => {
    if (!isVisible) return
    tl.set('.setHide', hideNow, { duration: 0 })
      .addLabel('left')
      .from($iconSamples.current, { autoAlpha: 0, delay: 0 })
      .addLabel('first')
      .from($circle1.current, fadeIn, '+=0.1')
      .from($label1.current, fadeIn, '-=0.3')
      .addLabel('second')
      .from($circle2.current, { autoAlpha: 0 }, '+=0.1')
      .from($label2.current, { autoAlpha: 0 }, '-=0.3')
      .addLabel('third')
      .from($circle3.current, { autoAlpha: 0 }, '+=0.1')
      .from($label3.current, { autoAlpha: 0 }, '-=0.3')
      .addLabel('right')
      .from($arrowRight.current, { autoAlpha: 0 }, '+=0.1')
      .from($iconAnalytics.current, { autoAlpha: 0 }, '-=0.3')
  }, [isVisible])

  const handleMouseEnter = debounce(n => {
    if (completed) {
      gsap
        .timeline()
        .to(groups[0].line.current, {
          autoAlpha: 0,
          scaleY: 0,
          duration: 0.15,
          immediateRender: true
        })
        .to(groups[0].copy.current, copyOut, '<')
        .to(groups[1].line.current, lineOut, '<')
        .to(groups[1].copy.current, copyOut, '<')
        .to(groups[2].line.current, lineOut, '<')
        .to(groups[2].copy.current, copyOut, '<')
        .fromTo(
          groups[n - 1].line.current,
          { autoAlpha: 0, scaleY: 0, duration: 0.3 },
          { autoAlpha: 1, scaleY: 1 }
        )
        .fromTo(
          groups[n - 1].copy.current,
          { autoAlpha: 0, duration: 0.3 },
          { autoAlpha: 1, scaleY: 1 },
          '<'
        )
    }
  }, 250)

  const handleMouseLeave = n => {
    if (completed) {
      gsap
        .timeline()
        .to(groups[0].line.current, {
          autoAlpha: 0,
          scaleY: 0,
          immediateRender: true,
          duration: 0.1
        })
        .to(groups[0].copy.current, hideNow, '<')
        .to(
          groups[1].line.current,
          { autoAlpha: 0, scaleY: 0, immediateRender: true, duration: 0.1 },
          '<'
        )
        .to(groups[1].copy.current, hideNow, '<')
        .to(
          groups[2].line.current,
          { autoAlpha: 0, scaleY: 0, immediateRender: true, duration: 0.1 },
          '<'
        )
        .to(groups[2].copy.current, hideNow, '<')
    }
  }

  return (
    <div
      className={styles.diagram}
      ref={$ref}
      style={{ visibility: isVisible ? 'visible' : 'hidden' }}
    >
      <svg
        className={styles.figure}
        width={1121}
        height={393}
        viewBox="0 0 1121 393"
        fill="none"
        {...props}
      >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M23.142 95H2v9.169h21.142V95zM2 93H0v13.169H3.594v79.82h17.958V106.169h3.59V93H2zm3.594 13.578h13.958v55.863H5.594v-9.976H9.58v-2H5.594v-13.562H9.58v-2H5.594v-13.564H9.58v-2H5.594v-12.761zm0 77.411v-19.548h13.958v19.548H5.594zM57.86 95H36.72v9.169H57.86V95zM36.72 93h-2v13.169h3.593v79.82h17.959V106.169h3.59V93H36.718zm3.593 13.578h13.959v55.863H40.312v-9.976H44.3v-2h-3.986v-13.562h3.986v-2h-3.986v-13.564h3.986v-2h-3.986v-12.761zm0 77.411v-19.548h13.959v19.548H40.312zM149.666 139.238l-7.311-7.311 1.414-1.415 8.78 8.781.707.707-.707.707-8.78 8.781-1.414-1.415 6.835-6.835H84.75v-2h64.916z"
          fill="#04F"
          ref={$iconSamples}
        />

        <g>
          <circle cx={318} cy={140} r={139} stroke="#04F" strokeWidth={2} ref={$circle1} />

          <text className={styles.label} ref={$label1}>
            <tspan x={279.005} y={126.727}>
              Library{' '}
            </tspan>
            <tspan x={253.249} y={158.727}>
              preparation
            </tspan>
          </text>

          <path
            stroke="#000"
            strokeWidth={1.561}
            d="M318.78 233v92"
            ref={$line1}
            className="setHide"
          />

          <text className={cn('setHide', styles.copy)} ref={$copy1}>
            <tspan x={189.851} y={360.545}>
              Cost-efficient library preparation{' '}
            </tspan>
            <tspan x={193.086} y={386.545}>
              that requires minimal DNA input
            </tspan>
          </text>

          <circle
            cx={318}
            cy={140}
            r={139}
            onMouseEnter={() => handleMouseEnter(1)}
            onMouseLeave={() => handleMouseLeave(1)}
            fill="transparent"
            className={styles.area}
          />
        </g>

        <g>
          <circle cx={550} cy={140} r={139} stroke="#04F" strokeWidth={2} ref={$circle2} />

          <text className={styles.label} ref={$label2}>
            <tspan x={483.543} y={145.727}>
              Sequencing
            </tspan>
          </text>

          <path
            stroke="#000"
            strokeWidth={1.561}
            d="M550.78 233v92"
            ref={$line2}
            className="setHide"
          />

          <text className={cn('setHide', styles.copy)} ref={$copy2}>
            <tspan x={369.986} y={360.545}>
              Sequencing at a low coverage with Illumina or{' '}
            </tspan>
            <tspan x={383.167} y={386.545}>
              BGI short read paired-end 100bp or 150bp
            </tspan>
          </text>

          <circle
            cx={550}
            cy={140}
            r={139}
            onMouseEnter={() => handleMouseEnter(2)}
            onMouseLeave={() => handleMouseLeave(2)}
            fill="transparent"
            className={styles.area}
          />
        </g>

        <g>
          <circle cx={782} cy={140} r={139} stroke="#04F" strokeWidth={2} ref={$circle3} />

          <text className={styles.label} ref={$label3}>
            <tspan x={711.737} y={126.727}>
              Imputation &{' '}
            </tspan>
            <tspan x={734.101} y={158.727}>
              Analysis
            </tspan>
          </text>

          <path
            stroke="#000"
            strokeWidth={1.561}
            d="M782.78 233v92"
            ref={$line3}
            className="setHide"
          />

          <text className={cn('setHide', styles.copy)} ref={$copy3}>
            <tspan x={680.462} y={360.545}>
              Gencove's imputation and{' '}
            </tspan>
            <tspan x={715.357} y={386.545}>
              analysis platform
            </tspan>
          </text>

          <circle
            cx={782}
            cy={140}
            r={139}
            fill="transparent"
            onMouseEnter={() => handleMouseEnter(3)}
            onMouseLeave={() => handleMouseLeave(3)}
            className={styles.area}
          />
        </g>

        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M1011.26 139.238l-7.31-7.311 1.41-1.415 8.78 8.781.71.707-.71.707-8.78 8.781-1.41-1.415 6.83-6.835h-64.436v-2h64.916z"
          fill="#04F"
          ref={$arrowRight}
        />

        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M1118.88 171.411h-63.04V94H1118.88v77.411zm-65.04 2h54.07v12.172h-65.04v-79.411h10.97v67.239zm0-69.239V92H1120.88v81.411h-10.97v14.172h-69.04v-83.411h12.97zm19.15 33.323v17.958h-2v-17.958h2zm9.58 17.958V126.72h-2v28.733h2zm9.58-40.705v40.705h-2v-40.705h2zm9.58 40.705V135.1h-2v20.353h2z"
          fill="#04F"
          ref={$iconAnalytics}
        />
      </svg>
    </div>
  )
}

function Components(props) {
  return (
    <div className={styles.wrap}>
      <div className="container">
        <h3 className={styles.title}>How low-pass sequencing works</h3>
        <Diagram width="100%" className="d-none d-md-block" />
        <img
          src="img/components-m.png"
          alt="components"
          className={cn('d-md-none', styles.componentsMobile)}
        />
      </div>
    </div>
  )
}

export default Components
