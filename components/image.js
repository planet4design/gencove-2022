import styles from 'styles/image.module.scss'
import cn from 'classnames'
import { gsap } from 'gsap'
import { useInView } from 'react-hook-inview'

const Image = ({ src, alt = '', ...rest }) => {
  const [ref, inView] = useInView({ threshold: 0.75, unobserveOnEnter: true })

  return (
    <div ref={ref} className={cn(styles.imageWrap, inView && styles.inView)} {...rest}>
      <img src={src} alt={alt} className={styles.img} />
    </div>
  )
}

export default Image
