import Head from 'next/head'

import Header from 'components/header'
import Hero from 'components/hero'
import Features from 'components/features'
import Enterprise from 'components/enterprise'
import Numbers from 'components/home-numbers'
import Vertical from 'components/vertical'
import Vision from 'components/home-vision'
import Partners from 'components/partners'
import News from 'components/news'
import PreFooter from 'components/pre-footer'
import Footer from 'components/footer'


import { text1, text2, logos } from 'data/partners-home.json'

function Home() {
  return (
    <>
      <Head>
        <title>Industrial-scale genome sequencing | Gencove</title>
        <meta
          name="description"
          content="Gencove’s low-pass sequencing platform is becoming the new standard for high-throughput genomics research and diagnostics across species."
        />
        <meta property="og:image" content={`${process.env.BASE_PATH_URL}img/og-image-home.jpg`} />
      </Head>

      <main className="mh-screen">
        <Header />
        <Hero />
        <Features />
        <Enterprise />
        <Numbers />
        <Vertical />
        <Vision />
        <Partners text1={text1} text2={text2} logos={logos} variant={'index'} />
        <News />
      </main>

      <PreFooter variant={'index'} />
      <Footer />
    </>
  )
}

export default Home
