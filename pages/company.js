import Head from 'next/head'

import Header from 'components/header'
import Hero from 'components/hero-company'
import Content from 'components/company-content1'
import Quote1 from 'components/company-quote1'
import Content2 from 'components/company-content2'
import Quote2 from 'components/company-quote2'
import Partners from 'components/company-partners'
import Quote3 from 'components/company-quote3'
import Content3 from 'components/company-content3'
import News from 'components/news'
import PreFooter from 'components/pre-footer'
import Footer from 'components/footer'

import { text1, logos } from 'data/partners-about.json'

function Company() {
  return (
    <>
      <Head>
        <title>Company | Gencove</title>
        <meta
          name="description"
          content="TITLE"
        />
        <meta property="og:image" content={`${process.env.BASE_PATH_URL}img/og-image-about.jpg`} />
      </Head>

      <main className="mh-screen">
        <Header />
        <Hero />
        <Content />
        <Quote1 />
        <Content2 />
        <Quote2 />
        <Partners />
        <Quote3 />
        <Content3 />
        <News />
      </main>
      <PreFooter variant={'about'} />
      <Footer />
    </>
  )
}

export default Company
