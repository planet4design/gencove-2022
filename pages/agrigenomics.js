import Head from 'next/head'

import Header from 'components/header'
import Hero from 'components/agri-hero'
import Content from 'components/agri-content1'
import Numbers from 'components/agri-numbers'
import Content2 from 'components/agri-content2'
import Components from 'components/agri-components'
import Content3 from 'components/agri-content3'
import News from 'components/agri-news'
import PreFooter from 'components/pre-footer'
import Footer from 'components/footer'

import { text1, logos } from 'data/partners-about.json'

function About() {
  return (
    <>
      <Head>
        <title>Agrigenomics | Gencove</title>
        <meta
          name="description"
          content="TITLE"
        />
        <meta property="og:image" content={`${process.env.BASE_PATH_URL}img/og-image-about.jpg`} />
      </Head>

      <main className="mh-screen">
        <Header />
        <Hero />
        <Content />
        <Numbers />
        <Content2 />
        <Components />
        <Content3 />
        <News />
      </main>
      <PreFooter variant={'about'} />
      <Footer />
    </>
  )
}

export default About
