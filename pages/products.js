import Head from 'next/head'

import Header from 'components/header'
import Hero from 'components/prod-hero'
import Content from 'components/prod-content1'
import Components from 'components/prod-components'
import Content2 from 'components/prod-content2'
import Pricing from 'components/prod-pricing'
import Content3 from 'components/prod-content3'
import Content4 from 'components/prod-content4'
import News from 'components/prod-news'
import PreFooter from 'components/pre-footer'
import Footer from 'components/footer'

import { text1, logos } from 'data/partners-about.json'

function Products() {
  return (
    <>
      <Head>
        <title>Products | Gencove</title>
        <meta
          name="description"
          content="TITLE"
        />
        <meta property="og:image" content={`${process.env.BASE_PATH_URL}img/og-image-about.jpg`} />
      </Head>

      <main className="mh-screen">
        <Header />
        <Hero />
        <Content />
        <Components />
        <Content2 />
        <Pricing />
        <Content3 />
        <Content4 />
        <News />
      </main>
      <PreFooter variant={'about'} />
      <Footer />
    </>
  )
}

export default Products
