import Head from 'next/head'

import Header from 'components/header'
import Hero from 'components/about-hero'
import Content from 'components/content1'
import Content2 from 'components/content2'
import Content3 from 'components/content3'
import PreFooter from 'components/pre-footer'
import Footer from 'components/footer'

import { text1, logos } from 'data/partners-about.json'

function About() {
  return (
    <>
      <Head>
        <title>About | Gencove</title>
        <meta
          name="description"
          content="TITLE"
        />
        <meta property="og:image" content={`${process.env.BASE_PATH_URL}img/og-image-about.jpg`} />
      </Head>

      <main className="mh-screen">
        <Header />
        <Hero />
        <Content />
        <Content2 />
        <Content3 />
      </main>
      <PreFooter variant={'about'} />
      <Footer />
    </>
  )
}

export default About