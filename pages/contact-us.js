import Head from 'next/head'
import Header from 'components/header'
import ContactUs from 'components/contact'
import Footer from 'components/footer'

function Contact() {
  return (
    <>
      <Head>
        <title>Contact Us | Gencove</title>
        <meta
          name="description"
          content="Start leveraging the power of sequencing. Get access to whole genome information at the cost of genotyping arrays. "
        />
        <meta
          property="og:image"
          content={`${process.env.BASE_PATH_URL}img/og-image-contact.jpg`}
        />
      </Head>

      <Header />

      <ContactUs />

      <Footer />
    </>
  )
}

export default Contact
