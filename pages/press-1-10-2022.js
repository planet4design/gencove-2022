import Head from 'next/head'

import Header from 'components/header'
import Hero from 'components/press-hero'
import Content from 'components/press-content1'
import PreFooter from 'components/pre-footer'
import Footer from 'components/footer'

import { text1, logos } from 'data/partners-about.json'

function Press() {
  return (
    <>
      <Head>
        <title>About | Gencove</title>
        <meta
          name="description"
          content="TITLE"
        />
        <meta property="og:image" content={`${process.env.BASE_PATH_URL}img/og-image-about.jpg`} />
      </Head>

      <main className="mh-screen">
        <Header />
        <Hero />
        <Content />
      </main>
      <PreFooter variant={'about'} />
      <Footer />
    </>
  )
}

export default Press


