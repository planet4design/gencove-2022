import Head from 'next/head'

import Header from 'components/header'
import Hero from 'components/human-hero'
import Content from 'components/human-content1'
import Content2 from 'components/human-content2'
import Components from 'components/human-components'
import Content3 from 'components/human-content3'
import Partners from 'components/human-partners'
import News from 'components/human-news'
import PreFooter from 'components/pre-footer'
import Footer from 'components/footer'

import { text1, logos } from 'data/partners-about.json'

function About() {
  return (
    <>
      <Head>
        <title>Human Genomics | Gencove</title>
        <meta
          name="description"
          content="TITLE"
        />
        <meta property="og:image" content={`${process.env.BASE_PATH_URL}img/og-image-about.jpg`} />
      </Head>

      <main className="mh-screen">
        <Header />
        <Hero />
        <Content />
        <Content2 />
        <Components />
        <Content3 />
        <Partners />
        <News />
      </main>
      <PreFooter variant={'about'} />
      <Footer />
    </>
  )
}

export default About
